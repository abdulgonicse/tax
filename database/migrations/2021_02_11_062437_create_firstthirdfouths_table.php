<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFirstthirdfouthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firstthirdfouths', function (Blueprint $table) {
            $table->id();
            $table->string('assessment_year')->nullable();
            $table->tinyinteger('return_submitted_yes')->nullable();
            $table->tinyinteger('return_submitted_no')->nullable();
            $table->string('name_of_the_assessee')->nullable();
            $table->tinyinteger('male')->nullable();
            $table->tinyinteger('female')->nullable();
            $table->tinyinteger('others')->nullable();
            $table->string('new_tin')->nullable();
            $table->string('old_tin')->nullable();
            $table->string('circle')->nullable();
            $table->string('zone')->nullable();
            $table->tinyinteger('resident')->nullable();
            $table->tinyinteger('non_resident')->nullable();
            $table->tinyinteger('freedom_figher')->nullable();
            $table->tinyinteger('disability')->nullable();
            $table->tinyinteger('aged_more')->nullable();
            $table->tinyinteger('egal_guardian')->nullable();
            $table->date('date')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('if_employed')->nullable();
            $table->string('spouse_Name')->nullable();
            $table->string('spouse_Tin')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mather_name')->nullable();
            $table->text('present_address')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('contact_telephone')->nullable();
            $table->string('email')->nullable();
            $table->string('nid')->nullable();
            $table->string('business_identification_number')->nullable();
            //3rd pages
            $table->tinyInteger('If_your_a_parent_yes')->nullable();
            $table->tinyInteger('If_your_a_parent_no')->nullable();
            $table->tinyInteger('are_you_required_yes')->nullable();
            $table->tinyInteger('are_you_required_no')->nullable();
            $table->tinyInteger('schedules_annexed_24a')->nullable();
            $table->tinyInteger('schedules_annexed_24b')->nullable();
            $table->tinyInteger('schedules_annexed_24c')->nullable();
            $table->tinyInteger('schedules_annexed_24d')->nullable();
            $table->tinyInteger('statement_annexed_it_10B2016')->nullable();
            $table->tinyInteger('statement_annexed_it_10BB2016')->nullable();
            $table->tinyInteger('other_statement')->nullable();
            $table->string('challan_no')->nullable();
            $table->string('day')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('income_tax_computation_sheet')->nullable();
            $table->string('salary_certificate')->nullable();
            $table->string('bank_statement')->nullable();
            $table->string('name')->nullable();
            $table->string('signature_place')->nullable();
            $table->string('return_submitted_under')->nullable();
            $table->string('tax_Office_entry_number')->nullable();

            ///4th no page

            $table->string('total_income_show_34')->nullable();
            $table->string('amount_payable_serial_34')->nullable();
            $table->string('amount_payable_serial_41')->nullable();
            $table->string('amount_paid_46')->nullable();
            $table->string('amount_of_net_wealth_shown')->nullable();
            $table->string('amount_of_net_wealth_surcharge_paid')->nullable();
            $table->string('contact_number_of_tax_Office')->nullable();
            $table->string('taxt_office_entry_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('firstthirdfouths');
    }
}
