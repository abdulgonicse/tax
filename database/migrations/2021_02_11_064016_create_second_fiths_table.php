<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSecondFithsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('second_fiths', function (Blueprint $table) {
            $table->id();
             //second_page
             $table->integer('salaies_annex_schedule_24a')->nullable();
             $table->integer('interest_on_securities')->nullable();
             $table->integer('income_from_house_property')->nullable();
             $table->integer('agricultural_income')->nullable();
             $table->integer('income_from_business_or_profession')->nullable();
             $table->integer('capital_gains')->nullable();
             $table->integer('income_from_other_sources')->nullable();
             $table->integer('share_of_income_from_firm_or_aop')->nullable();
             $table->integer('income_of_minor_or_spouse')->nullable();
             $table->integer('foreign_income')->nullable();
             $table->integer('total_income')->nullable();
 
             //second_page ***Tax Computation and Payment
 
             $table->integer('gross_tax_before_tax_rebate')->nullable();
             $table->integer('tax_rebate')->nullable();
             $table->integer('net_tax_after_tax_rebate')->nullable();
             $table->integer('minimum_tax')->nullable();
             $table->integer('net_wealth_surchange')->nullable();
             $table->integer('interest_or_any_other')->nullable();
             $table->integer('total_amount_payable')->nullable();
             $table->integer('tax_deducted')->nullable();
             $table->integer('advance_tax_paid')->nullable();
             $table->integer('adjustment_of_tax')->nullable();
             $table->integer('amount_paid')->nullable();
             $table->integer('total_amount')->nullable();
             $table->integer('deficit_or_excess_refunddable')->nullable();
             $table->string('tax_exempted')->nullable();
 
             //5th page
             $table->string('basic_pay')->nullable();
             $table->string('special_pay')->nullable();
             $table->string('arrear_pay')->nullable();
             $table->string('dearness_allowance')->nullable();
             $table->string('house_rent_allowance')->nullable();
             $table->string('medicale_allowance')->nullable();
             $table->string('conveyance_allowance')->nullable();
             $table->string('festival_allowance')->nullable();
             $table->string('allowance_for_support_staff')->nullable();
             $table->string('leave_allowance')->nullable();
             $table->string('honorarium_reward_fee')->nullable();
             $table->string('overtime_allowance')->nullable();
             $table->string('bonus_ex_gratia')->nullable();
             $table->string('other_allowances')->nullable();
             $table->string('employers_contribution')->nullable();
             $table->string('interest_accrued')->nullable();
             $table->string('deemed_income')->nullable();
             $table->string('deemed_income_for')->nullable();
             $table->string('other_if_any')->nullable();
             $table->string('total')->nullable();
 
             $table->string('amount')->nullable();
             $table->string('taxt_exempted')->nullable();
             $table->string('taxable')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('second_fiths');
    }
}
