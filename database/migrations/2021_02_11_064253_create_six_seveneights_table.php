<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSixSeveneightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('six_seveneights', function (Blueprint $table) {
            $table->id();
            $table->string('address_of_the_property')->nullable();
            $table->string('total_area')->nullable();
            $table->string('share_of_the_asessee')->nullable();
            $table->string('annual_value')->nullable();
            $table->string('repair_collection_etc')->nullable();
            $table->string('municipal_or_loacal_tax')->nullable();
            $table->string('land_revenue')->nullable();
            $table->string('interest_on_loan')->nullable();
            $table->string('income_from_house_property')->nullable();
            $table->string('incase_of_partial')->nullable();
            $table->string('provide_additional')->nullable();
            $table->string('income_of_from_house_property_1')->nullable();
            $table->string('income_of_from_house_property_2')->nullable();
            $table->string('income_of_from_house_property_3')->nullable();

            ///7th page
            $table->string('trade_licence')->nullable();
            $table->string('address')->nullable();
            $table->string('sales_turnover_receipts')->nullable();
            $table->string('gross_profit')->nullable();
            $table->string('general_administrative')->nullable();
            $table->string('net_profit')->nullable();
            $table->string('cash_in_hand_at_bank')->nullable();
            $table->string('inventories')->nullable();
            $table->string('fixed_assets')->nullable();
            $table->string('other_assests')->nullable();
            $table->string('total_asset')->nullable();
            $table->string('opening_capital')->nullable();
            $table->string('summary_net_profit')->nullable();
            $table->string('withdrawals_in_the_income_year')->nullable();
            $table->string('closing_capital')->nullable();
            $table->string('liabilities')->nullable();
            $table->string('total_capital_and_liabilities')->nullable();

            ///8th page
            $table->string('life_insurance_premium')->nullable();
            $table->string('contribution_to_deposit')->nullable();
            $table->string('investment_in_approvedsaving_certificate')->nullable();
            $table->string('investment_in_approvedsaving')->nullable();
            $table->string('contribution_to_provident')->nullable();
            $table->string('self_contribution')->nullable();
            $table->string('contribution')->nullable();
            $table->string('contribution_to_benevolent')->nullable();
            $table->string('contribution_to_zakat_fund')->nullable();
            $table->string('others_if_any_give_details')->nullable();
            $table->string('total_allowable_investment')->nullable();
            $table->string('eligible_amount_for_rebate')->nullable();
            $table->string('total_allowable_investments')->nullable();
            $table->string('total_income')->nullable();
            $table->string('crore')->nullable();
            $table->string('amount_of_tax_rebate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('six_seveneights');
    }
}
