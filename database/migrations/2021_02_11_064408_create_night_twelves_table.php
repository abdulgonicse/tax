<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNightTwelvesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('night_twelves', function (Blueprint $table) {
            $table->id();
            //9th
            $table->string('business_capital_other_than_05_b')->nullable();
            $table->string('directors_shareholdings_in_limited_companies')->nullable();
            $table->string('non_agricultural_property')->nullable();
            $table->string('advance_made_for_non_agricultural_property')->nullable();
            $table->string('agricultural_property')->nullable();
            $table->string('share_debentures_etc')->nullable();
            $table->string('life_insurance_premium')->nullable();
            $table->string('fixed_deposit_term_deposits_and_dps')->nullable();
            $table->string('loans_given_to_others_mention_name_and_tin')->nullable();
            $table->string('other_financial_assets_give_deatils')->nullable();
            $table->string('motro_cars')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('engine_cc')->nullable();
            $table->string('registration_no')->nullable();
            $table->string('mention_quantity')->nullable();
            $table->string('furniture_equipment_and_electronics_items')->nullable();
            $table->string('other_assets_of_significant_value_jewellery')->nullable();
            $table->string('cash_and_fund_outside_business_13A_13B_13C_13D')->nullable();
            $table->string('notes_and_currencies')->nullable();
            $table->string('banks_cards_and_electronic_cash')->nullable();
            $table->string('provident_fund_and_ohter_fund')->nullable();
            $table->string('other_deposits_balance_and_advance_other_than_8')->nullable();
            $table->string('gross_wealth_aggregate_of_05_to_13')->nullable();
            $table->string('liabilities_outside_business_15A_15B_15C')->nullable();
            $table->string('borrowdings_from_banks_other_finincial_instirutions')->nullable();
            $table->string('unsecured_loadn_mention_name_and_tin')->nullable();
            $table->string('other_loans_or_overdrafts')->nullable();
            $table->string('net_wealth')->nullable();
            $table->string('net_wealth_at_the_last_date_of_the_pervious_income_year')->nullable();
            $table->string('change_in_net_wealth')->nullable();
            $table->string('other_fund_outflow_during_the_income_year_19A_19B_19C')->nullable();
            $table->string('annual_living_expenditure_and_tax_payments_as_it_10BB2016')->nullable();
            $table->string('loss_deductions_expenses_etc_not_mentioned_in_IT_10BB2016')->nullable();
            $table->string('total_fund_outflow_in_the_income_year_18_19')->nullable();
            $table->string('sources_of_fund_21A_21B_21C')->nullable();
            $table->string('income_shown_in_the_return')->nullable();
            $table->string('taxexempted_income_and_allowance')->nullable();
            $table->string('other_receipts_gift_from_father_mother')->nullable();
            $table->string('shortage_of_fund_if_any_2021')->nullable();

            //12 pages
            $table->string('sl')->nullable();
            $table->string('content')->nullable();
            $table->string('payment_of_tax_at_source')->nullable();
            $table->string('payment_of_tax_surcharge_or_other_amount')->nullable();
            $table->string('total_amount_of_expense_and_tax_12_13')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('night_twelves');
    }
}
