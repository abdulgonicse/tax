<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenPageFirstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ten_page_firsts', function (Blueprint $table) {
            $table->id();
            $table->string('shareholdings_in_limited_companies_as_director')->nullable();
            $table->string('no_of_shares')->nullable();
            $table->string('value')->nullable();
            $table->string('description_location_and_size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ten_page_firsts');
    }
}
