<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenPageThirdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ten_page_thirds', function (Blueprint $table) {
            $table->id();
            //10th page sub table Shareholdings in limited companies as director
            $table->string('agricultural_property_at_cost_value')->nullable();
            $table->string('value_at_the_start_of_income_year')->nullable();
            $table->string('increased_decreased_during_the_income_year')->nullable();
            $table->string('value_at_the_last_date_of_income_year')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ten_page_thirds');
    }
}
