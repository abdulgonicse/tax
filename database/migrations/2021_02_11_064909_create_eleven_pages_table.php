<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElevenPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eleven_pages', function (Blueprint $table) {
            $table->id();
            //11th page
            $table->string('expenses_for_food_clothing_and')->nullable();
            $table->string('housing_expense')->nullable();
            $table->string('auto_and_transportation_expense')->nullable();
            $table->string('drivers_salary_fuel_and_maintenance_amount')->nullable();
            $table->string('drivers_salary_fuel_and_maintenance_comment')->nullable();
            $table->string('other_transportation_amount')->nullable();
            $table->string('other_transportation_comment')->nullable();
            $table->string('household_and_utility')->nullable();
            $table->string('electicity')->nullable();
            $table->string('gas_water_sewer_and_garbage')->nullable();
            $table->string('phone_internet_tv_channels_subscription')->nullable();
            $table->string('home_support_staff_and_other_expenses')->nullable();
            $table->string('childrens_wife_education_expenses')->nullable();
            //11 pages
            $table->string('special_expenses')->nullable();
            $table->string('sl')->nullable();
            $table->string('content')->nullable();
            $table->string('amount')->nullable();
            $table->string('comment')->nullable();
            $table->string('any_other_expenses')->nullable();
            $table->string('total_expense_relating_to_lifestyle')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eleven_pages');
    }
}
