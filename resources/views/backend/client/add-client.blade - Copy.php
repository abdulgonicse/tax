@extends('backend.layouts.master')
@section('content')
<style>
    * {
        margin: 0;
        padding: 0
    }

    html {
        height: 100%
    }

    #grad1 {
        background-color: #9C27B0;
        /* background-image: linear-gradient(120deg, #FF4081, #81D4FA) */
    }

    #msform {
        text-align: center;
        position: relative;
        margin-top: 20px
    }

    #msform fieldset .form-card {
        background: white;
        border: 0 none;
        border-radius: 0px;
        box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
        padding: 20px 40px 30px 40px;
        box-sizing: border-box;
        width: 94%;
        margin: 0 3% 20px 3%;
        position: relative
    }

    #msform fieldset {
        background: white;
        border: 0 none;
        border-radius: 0.5rem;
        box-sizing: border-box;
        width: 100%;
        margin: 0;
        padding-bottom: 20px;
        position: relative
    }

    #msform fieldset:not(:first-of-type) {
        display: none
    }

    #msform fieldset .form-card {
        text-align: left;
        /* color: #9E9E9E */
        font-size: 16px;
        line-height: 26px;

    }

    #msform .action-button {
        width: 100px;
        background: skyblue;
        font-weight: bold;
        color: white;
        border: 0 none;
        border-radius: 0px;
        cursor: pointer;
        padding: 10px 5px;
        margin: 10px 5px
    }

    #msform .action-button:hover,
    #msform .action-button:focus {
        box-shadow: 0 0 0 2px white, 0 0 0 3px skyblue
    }

    #msform .action-button-previous {
        width: 100px;
        background: #616161;
        font-weight: bold;
        color: white;
        border: 0 none;
        border-radius: 0px;
        cursor: pointer;
        padding: 10px 5px;
        margin: 10px 5px
    }

    #msform .action-button-previous:hover,
    #msform .action-button-previous:focus {
        box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
    }

    select.list-dt {
        border: none;
        outline: 0;
        border-bottom: 1px solid #ccc;
        padding: 2px 5px 3px 5px;
        margin: 2px
    }

    select.list-dt:focus {
        border-bottom: 2px solid skyblue
    }

    .card {
        z-index: 0;
        border: none;
        border-radius: 0.5rem;
        position: relative
    }

    .fs-title {
        font-size: 25px;
        color: #2C3E50;
        margin-bottom: 10px;
        font-weight: bold;
        text-align: left
    }

    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        color: lightgrey
    }

    #progressbar .active {
        color: #000000
    }

    #progressbar li {
        list-style-type: none;
        font-size: 12px;
        width: 7%;
        float: left;
        position: relative
    }

    #progressbar #account:before {
        font-family: FontAwesome;
        content: "\f023"
    }

    #progressbar #personal:before {
        font-family: FontAwesome;
        content: "\f007"
    }

    #progressbar #payment:before {
        font-family: FontAwesome;
        content: "\f09d"
    }

    #progressbar #confirm:before {
        font-family: FontAwesome;
        content: "\f00c"
    }

    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 18px;
        color: #ffffff;
        background: lightgray;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px
    }

    #progressbar li:after {
        content: '';
        width: 100%;
        height: 2px;
        background: lightgray;
        position: absolute;
        left: 0;
        top: 25px;
        z-index: -1
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: skyblue
    }

    .radio-group {
        position: relative;
        margin-bottom: 25px
    }

    .radio {
        display: inline-block;
        width: 204;
        height: 104;
        border-radius: 0;
        background: lightblue;
        box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
        box-sizing: border-box;
        cursor: pointer;
        margin: 8px 2px
    }

    .radio:hover {
        box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
    }

    .radio.selected {
        box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
    }

    .fit-image {
        width: 100%;
        object-fit: cover
    }

    .box-sm-box {
        width: 40px !important;
        height: 40px !important;
        border: 1px solid #dee2e6 !important;
        display: flex !important;
        align-items: center !important;
        justify-content: center !important;
        text-align: center !important;
        line-height: 40px !important;
    }

    .form-group .form-control:focus::placeholder {
        opacity: 0;
    }
</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row justify-content-center ">
                <div class="col-11 col-sm-9 col-md-7 col-lg-12 text-center p-0 mt-3 mb-2">
                    <div class="card px-0 pt-4 pb-0 mt-2 mb-2">
                        <h2><strong>Sign Up Your User Account</strong></h2>
                        <p>Fill all form field to go to next step</p>
                        <div class="row">
                            <div class="col-md-12 mx-0">
                                <form id="msform">
                                    <ul id="progressbar">
                                        <li class="active" id="account"><strong>Account</strong></li>
                                        <li id="personal"><strong>Personal</strong></li>
                                        <li id="payment"><strong>Payment</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                    </ul>

                                    <!-- start Individual Assessee -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">Basic Information</h2>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            01. Assessment Year
                                                        </label>

                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="0">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            02. Return submitted under section 82BB(Select One)
                                                        </label>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" value="Yes" name="return_submitted_under_section_82BB" id="inlineRadio1" value="option1">
                                                            <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" value="No" name="return_submitted_under_section_82BB" id="inlineRadio2" value="option2">
                                                            <label class="form-check-label" for="inlineRadio2">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            03. Name of the Assessee
                                                        </label>
                                                        <input type="text" name="name_of_the_assessee" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            04. Gender
                                                        </label>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" value="Male" type="radio" name="gender" id="male" >
                                                            <label class="form-check-label" for="male">Male</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" value="Female" type="radio" name="gender" id="female">
                                                            <label class="form-check-label" for="female">Female</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input"  type="radio" name="gender" id="others" value="others">
                                                            <label class="form-check-label" for="others">Others</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            05. 12-digit Tin:
                                                        </label>
                                                        <input type="text" name="new_tin" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            06. Old Tin:
                                                        </label>
                                                        <input type="text" name="old_tin" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            07. Circle:
                                                        </label>
                                                        <input type="text" name="circle" class="form-control" placeholder="
                                                                                                                                                                                                                ">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            08. Zone:
                                                        </label>
                                                        <input type="text" name="zone" class="form-control" placeholder="
                                                                                                                                                                                                                ">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            09. Resident Status (Tick One)
                                                        </label>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" value="resident" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                            <label class="form-check-label" for="inlineRadio1">Resident</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" value="non-resident" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                            <label class="form-check-label" for="inlineRadio2">Non-Resident</label>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            11. Tick on the box(es) below if you are
                                                        </label>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" value="A
                                                                  Gazettedwarwoundedfreedomfigher" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">
                                                            <label class="form-check-label" for="inlineRadio1">A
                                                                Gazetted war-wounded freedom figher</label>
                                                        </div>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="2">
                                                            <label class="form-check-label" for="inlineRadio1">A person
                                                                with disability</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="3">
                                                            <label class="form-check-label" for="inlineRadio1">Aged 65
                                                                years or more</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="4">
                                                            <label class="form-check-label" for="inlineRadio2">A
                                                                parent/legal guardian of a person with
                                                                disability</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            11. Date of Birth (DD-MM-YYYY)
                                                        </label>
                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="5">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="4">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="9">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="7">
                                                                <input type="text" class="form-control box-sm-box" placeholder="2">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            12. Income Year
                                                        </label>
                                                        <div class="income-year-box d-flex">
                                                            <input type="date" name="date">
                                                            <span class="mx-3 font-bold">to</span>
                                                            <input type="date">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            13. If Employed
                                                        </label>
                                                        <input type="text" name="if_employed" class="form-control" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            14. Spouse Name:
                                                        </label>
                                                        <input type="text" name="spouse_Name" class="form-control" placeholder="Enter Spouse Name">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            15. Spouse TIN (if any)
                                                        </label>
                                                        <input type="text" name="spouse_Tin" class="form-control" placeholder="Enter Spouse TIN">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            16. Father's Name:
                                                        </label>
                                                        <input type="text" name="father_name" class="form-control" placeholder="Enter Father's Name">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            17. Mother's Name:
                                                        </label>
                                                        <input type="text" name="mather_name" class="form-control" placeholder="Enter Mother's Name">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            18. Present Address:
                                                        </label>
                                                        <input type="text" name="present_address" class="form-control" placeholder="Enter Present Address">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            19. Permanent Address:
                                                        </label>
                                                        <input type="text" name="permanent_address" class="form-control" placeholder="Enter Permanent Address">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            20. Contact Telephone:
                                                        </label>
                                                        <input type="number" name="contact_telephone" class="form-control" placeholder="Enter Contact Telephone">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            21. E-Mail:
                                                        </label>
                                                        <input type="email" name="email" class="form-control" placeholder="Enter E-Mail">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            22. National Identification Number:
                                                        </label>
                                                        <input type="text" name="nid" class="form-control" placeholder="Enter NID">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            23. Business Identification Number(s):
                                                        </label>
                                                        <input type="text" name="business_identification_number" class="form-control" placeholder="Enter Business Identification Number">
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                            </div>
                                            <!--  end row -->



                                        </div>
                                        <input type="button" name="next" class="next action-button" value="Next Step" />

                                    </fieldset>
                                    <!-- end Individual Assessee -->

                                    <!-- start particulars of income tax  -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">Particulars Of Income and
                                                Tax</h2>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="mb-2 d-block">
                                                    </label>
                                                    <div class="multiple-input-box d-flex ">Tin:
                                                        <div class="part1 d-flex mr-3">
                                                            <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                            <input type="text" class="form-control box-sm-box mr-1" placeholder="5">
                                                            <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                            <input type="text" class="form-control box-sm-box mr-1" placeholder="4">
                                                            <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                            <input type="text" class="form-control box-sm-box mr-1" placeholder="9">
                                                            <input type="text" class="form-control box-sm-box mr-1" placeholder="7">
                                                            <input type="text" class="form-control box-sm-box" placeholder="2">
                                                            <input type="text" class="form-control box-sm-box" placeholder="2">
                                                            <input type="text" class="form-control box-sm-box" placeholder="2">
                                                            <input type="text" class="form-control box-sm-box" placeholder="2">
                                                            <input type="text" class="form-control box-sm-box" placeholder="2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                24. Salaies (annex Schedule 24A)
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                25. Interest on securities
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                26. Income from house property(annex Schedule 24B)
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                27. Agricultural income
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                28. Income from business or profession (annex Schedule
                                                                24c)
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                29. Capital gains
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                30. Income from other sources
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                31. Share of income from firm or AOP
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                32. Income of minor or spouse under section 43(4)
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                33. Foreign income
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                34. Total income (aggregate of 24 to 33)
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>

                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">Tax Computation and Payment
                                            </h2>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                35. Gross tax before tax rebate
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                36. Tax rebate (annex Schedule 24D)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                37. Net tax after tax rebate
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                38. Minimum tax
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                39. Net wealth surchange
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                40. Interest or any other amount under the Ordinance (if
                                                                any)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                41. Total amount payable
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                42. Tax deducted or collected at source (attach proof)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                43. Advance tax paid (attach proof)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                44. Adjustment of tax refund [mention assessment year(s)
                                                                of refund]
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                45. Amount paid with return (attach proof) U/S 74
                                                                (Challan No. Dated: 11/2019, Mohakhali Branch, Sonali
                                                                Bank Limited)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                46. Total amount paid and adjusted (42+43+44+45)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                47. Deficit or excess (refunddable)(42+43+44+45)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-9">
                                                            <label class="mb-2">
                                                                48. Tax exempted
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <input type="number" class="form-control " placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <!-- end particulars of income tax  -->

                                    <!-- start instuction , enclosure and verification  -->
                                    <fieldset>
                                        <div class="form-card">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-12">
                                                            <label class="">
                                                                49. Instructions
                                                                1. Statement of assets, liabilities and expenses
                                                                (IT-10B2016) and statement of life style expense (IT-
                                                                10BB2016) must be furnished with the return unless you
                                                                are exempted from furnishing such statement(s) under
                                                                section 80.
                                                                <br />2. Proof of payments of tax, including Advance tax
                                                                and withholding tax and the proof of investment for tax
                                                                rebate must be provided along with return. <br />
                                                                3. Attach account statements and other documents where
                                                                applicable
                                                            </label>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <div class="form-group">

                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                50. If your a parent of a person with disability, has
                                                                your spouse availed the extended tax exemption
                                                                threshold?(tick one)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                                <label class="form-check-label" for="inlineRadio2">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <div class="form-group">

                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                50. Are you required to submit a statement of assets,
                                                                liabilities and expenses (IT-10b2016) under section
                                                                80(1) ? (tick one)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                                <label class="form-check-label" for="inlineRadio2">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">

                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                51. Are you required to submit a statement of assets,
                                                                liabilities and expenses (IT-10b2016) under section
                                                                80(1) ? (tick one)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                                <label class="form-check-label" for="inlineRadio2">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <div class="form-group">

                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-12">
                                                            <label class="mb-2">
                                                                52. Schedules annexed (tick all that are applicable)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                                <label class="form-check-label" for="inlineRadio1">24A</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                                <label class="form-check-label" for="inlineRadio2">24B</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                                <label class="form-check-label" for="inlineRadio2">24C</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                                <label class="form-check-label" for="inlineRadio2">24D</label>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">

                                                    <div class="row align-items-center justify-content-between">
                                                        <div class="col-lg-6">
                                                            <label class="mb-2">
                                                                53. Schedules annexed (tick all that are applicable)
                                                            </label>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                                <label class="form-check-label" for="inlineRadio1">IT-10B2016</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                                <label class="form-check-label" for="inlineRadio2">IT-10BB2016</label>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="mb-2 d-block">
                                                        54. Other Statement, documents, etc. attached (list all)
                                                    </label>

                                                    <div class="row align-items-center">
                                                        <div class="col-lg-6 d-flex align-items-center">
                                                            <label class="mr-2">1. Challan No</label>
                                                            <input type="number" class="form-control w-50" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-6 d-flex align-items-center">
                                                            <label class="mr-2">Expiry Date *</label>
                                                            <select class="list-dt" id="month" name="expmonth">
                                                                <option selected>Month</option>
                                                                <option>January</option>
                                                                <option>February</option>
                                                                <option>March</option>
                                                                <option>April</option>
                                                                <option>May</option>
                                                                <option>June</option>
                                                                <option>July</option>
                                                                <option>August</option>
                                                                <option>September</option>
                                                                <option>October</option>
                                                                <option>November</option>
                                                                <option>December</option>
                                                            </select>

                                                            <select class="list-dt" id="year" name="expyear">
                                                                <option selected>Year</option>
                                                                <option>2010</option>
                                                                <option>2010</option>
                                                                <option>2010</option>
                                                                <option>2010</option>
                                                                <option>2010</option>
                                                                <option>2010</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 d-flex align-items-center">
                                                            <label class="mr-2">2. Income Tax Computation Sheet</label>

                                                        </div>
                                                        <div class="col-lg-4 d-flex align-items-center">
                                                            <label class="mr-2">3. Salary Certificate</label>

                                                        </div>
                                                        <div class="col-lg-4 d-flex align-items-center">
                                                            <label class="mr-2">4. Bank statement</label>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="mb-2 d-block">
                                                        55. Verification I solemnly declare that to the best of my
                                                        knowledge and belief the information given in this retrn and
                                                        statements and documents annexed or attached here with are
                                                        correct and complete.
                                                    </label>

                                                    <div class="row align-items-center">
                                                        <div class="col-lg-6 d-flex align-items-center">
                                                            <label class="mr-2">Name:</label>
                                                            <input type="number" class="form-control w-50" placeholder="Enter Amount">
                                                        </div>
                                                        <div class="col-lg-6 d-flex align-items-center">
                                                            <label class="mr-2">Expiry Date *</label>
                                                            <input type="file" class="form-control w-50">


                                                        </div>
                                                        <br><br>
                                                        <div class="col-lg-8 d-flex align-items-center">
                                                            <label class="mr-2">Date of singnature (DD-MM-YY)</label>
                                                            <div class="multiple-input-box d-flex ">
                                                                <div class="part1 d-flex mr-3">
                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                    <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 d-flex align-items-center">
                                                            <label class="mr-2">Place of singnature</label>
                                                            <input type="number" class="form-control w-50" placeholder="Enter Amount">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12" style="text-align: center">
                                                <h3> For the official use only</h3>
                                                <h5>Return Submission Information</h5>
                                                <div class="form-group">
                                                    <label class="mb-2 d-block">

                                                    </label>

                                                    <div class="row align-items-center">

                                                        <div class="col-lg-7 d-flex align-items-center">
                                                            <label class="mr-2">Date of singnature (DD-MM-YY)</label>
                                                            <div class="multiple-input-box d-flex ">
                                                                <div class="part1 d-flex mr-3">
                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                    <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                    <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 d-flex align-items-center">
                                                            <label class="mr-2">Tax Office Entry Number</label>
                                                            <input type="number" class="form-control w-50" placeholder="Enter Amount">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <!-- end col -->
                                        </div>
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <!-- start instuction , enclosure and verification  -->

                                    <!-- start acknowledgement receipt of return of income  -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">Basic Information</h2>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            01. Assessment Year
                                                        </label>

                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            02. Return submitted under section 82BB(Select One)
                                                        </label>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                            <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                            <label class="form-check-label" for="inlineRadio2">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            03. Name of the Assessee
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>
                                                <!-- end col -->


                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            04. 12-digit Tin:
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            05. Old Tin:
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            07. Circle:
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="
                                                                                                                                                                                                                ">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            08. Tax Zone:
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="
                                                                                                                                                                                                                ">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            Amount Payable (Serial 34)
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            Amount Payable (Serial 41)
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            Amount Payable (Serial 46)
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            Amount of net wealth shown in IT 10B2016
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            Amount of net wealth surcharge paid
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">

                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">

                                                        </label>

                                                        <div class="row align-items-center">

                                                            <div class="col-lg-7 d-flex align-items-center">
                                                                <label class="mr-2">Date of singnature
                                                                    (DD-MM-YY)</label>
                                                                <div class="multiple-input-box d-flex ">
                                                                    <div class="part1 d-flex mr-3">
                                                                        <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                        <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                        <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                        <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                        <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                        <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-5 d-flex align-items-center">
                                                                <label class="mr-2">Tax Office Entry Number</label>
                                                                <input type="number" class="form-control w-50" placeholder="Enter Amount">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            singnature and seal of the offical receiving the return
                                                        </label>

                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            Date of signature
                                                        </label>
                                                        <input type="file" class="form-control" placeholder="Enter Amount">

                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            Contact Number of Tax Office
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Amount">
                                                    </div>
                                                </div>






                                                <!-- end col -->

                                            </div>
                                            <!--  end row -->



                                        </div>
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <!-- end acknowledgement receipt of return of income  -->

                                    <!-- start schedule 24A  -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">Schedule 24A</h2>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            01. Assessment Year
                                                        </label>

                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            02. Tin
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Tin">
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-12">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Sl No</th>
                                                                <th scope="col">Particulars</th>
                                                                <th scope="col">Amount</th>
                                                                <th scope="col">Tax Exempted</th>
                                                                <th scope="col">Taxable</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>03 </td>
                                                                <td class="w-50">
                                                                    Basic Pay
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <!-- end single item -->

                                                            <tr>
                                                                <td>04 </td>
                                                                <td class="w-50">
                                                                    Special Pay
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <!-- end single item -->

                                                            <tr>
                                                                <td>05 </td>
                                                                <td class="w-50">
                                                                    Arrear Pay (if not included in taxable income
                                                                    earlier)
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <!-- end single item -->

                                                            <tr>
                                                                <td>06 </td>
                                                                <td class="w-50">
                                                                    Dearness allowance
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <!-- end single item -->

                                                            <tr>
                                                                <td>07 </td>
                                                                <td class="w-50">
                                                                    House rent allowance
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>08 </td>
                                                                <td class="w-50">
                                                                    Medical allowance
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>09 </td>
                                                                <td class="w-50">
                                                                    Conveyance allowance
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>10 </td>
                                                                <td class="w-50">
                                                                    Festival Allowance
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>11 </td>
                                                                <td class="w-50">
                                                                    Allowance for support staff
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>12 </td>
                                                                <td class="w-50">
                                                                    Leave allowance
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>13 </td>
                                                                <td class="w-50">
                                                                    Honorarium/Reward/Fee
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>14 </td>
                                                                <td class="w-50">
                                                                    Overtime Allowance
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>15 </td>
                                                                <td class="w-50">
                                                                    Bonus / Ex-gratia
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>16 </td>
                                                                <td class="w-50">
                                                                    Other Allowances
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>17 </td>
                                                                <td class="w-50">
                                                                    Employer's contribution to a recognized provident
                                                                    fund
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>18 </td>
                                                                <td class="w-50">
                                                                    Interest accrued on a recognized provident provident
                                                                    fund
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>19 </td>
                                                                <td class="w-50">
                                                                    Deemed income income for transort facility
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <td>20 </td>
                                                                <td class="w-50">
                                                                    Deemed income for free furnished/unfurnished
                                                                    accommodation
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>



                                                            <tr>
                                                                <td>21 </td>
                                                                <td class="w-50">
                                                                    Other, if any (give detail)
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <td>22 </td>
                                                                <td class="w-50">
                                                                    Total
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td> </td>
                                                                <td class="w-50">
                                                                    All figures of amount are in taka ()
                                                                </td>
                                                                <td>

                                                                </td>
                                                                <td>

                                                                </td>
                                                                <td>

                                                                </td>
                                                            </tr>



                                                            <!-- end single item -->

                                                        </tbody>
                                                    </table>

                                                    <!-- start name & signature field -->
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="mb-2 d-block">
                                                                    Name
                                                                </label>
                                                                <input type="text" class="form-control" placeholder="Enter Name">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="mb-2 d-block">
                                                                    Signature & Date
                                                                </label>
                                                                <input type="file" class="form-control" placeholder="Enter Name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- start name & signature field -->

                                                </div>
                                            </div>
                                            <!--  end row -->

                                        </div>
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />

                                    </fieldset>
                                    <!-- end schedule 24A  -->

                                    <!-- start schedule 24B  -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">Schedule 24B</h2>
                                            <h6 style="text-align: center">Particulars of income from House Property
                                            </h6>
                                            <h6 style="text-align: center">Annex this Schedule to the return of income
                                                if you have income from House Property</h6>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            01. Assessment Year
                                                        </label>

                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            02. Tin
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Tin">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <label> For each House Property</label>
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th rowspan="4">03</th>
                                                            <th>Description of the house property</th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="w-25">03-A</td>
                                                                            <td class="w-50">
                                                                                Address of the property
                                                                            </td>
                                                                            <td class="w-25">
                                                                                <input type="text" class="form-control">
                                                                            </td>

                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>03-B</td>

                                                                            <td>
                                                                                Total Area
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>03-C</td>
                                                                            <td>
                                                                                Share of the asessee(%)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>

                                                <div class="col-lg-12">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col" class="w-10">Sl No</th>
                                                                <th scope="col " class="w-65">Content</th>
                                                                <th scope="col" class="w-25">Amount</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>04 </td>
                                                                <td>
                                                                    Annual Value
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <!-- end single item -->
                                                            <tr>
                                                                <td>05 </td>
                                                                <td colspan="3">
                                                                    Deducations

                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="w-25">05-A</td>
                                                                                <td class="w-50">
                                                                                    Repair, collection etc
                                                                                </td>
                                                                                <td class="w-25">
                                                                                    <input type="text" class="form-control">
                                                                                </td>

                                                                            </tr>
                                                                            <!-- end single item -->

                                                                            <tr>
                                                                                <td>035-B</td>
                                                                                <td>
                                                                                    Municipal or loacal Tax
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- end single item -->

                                                                            <tr>
                                                                                <td>05-C</td>
                                                                                <td>
                                                                                    Land Revenue
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- end single item -->
                                                                            <tr>
                                                                                <td>05-D</td>
                                                                                <td>
                                                                                    Interest on Loan/Mortage/capital charge
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- end single item -->
                                                                        </tbody>
                                                                    </table>

                                                                </td>

                                                            </tr>
                                                            <!-- end single item -->
                                                            <tr>
                                                                <td>06 </td>
                                                                <td>
                                                                    Income from house property(04-05)
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <!-- end single item -->
                                                            <tr>
                                                                <td>07 </td>
                                                                <td>
                                                                    Incase of partial ownership, the share of income
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <!-- end single item -->

                                                        </tbody>
                                                    </table>

                                                </div>

                                                <div class="col-lg-12">
                                                    <label> Provide information if income from more than one house property</label>
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th rowspan="4">08</th>
                                                            <td>aggregate of income of all house property (1+2+3=----) (provide additional papaers if necessary)</td>
                                                            <td class="w-25"><input type="text" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="w-25">1</td>
                                                                            <td class="w-50">
                                                                                (Income of from house property 1)
                                                                            </td>
                                                                            <td class="w-25">
                                                                                <input type="text" class="form-control">
                                                                            </td>

                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>2</td>

                                                                            <td>
                                                                                (Income of from house property 2)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>3</td>
                                                                            <td>
                                                                                (Income of from house property 3)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>

                                                    <!-- start name & signature field -->
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="mb-2 d-block">
                                                                    Name
                                                                </label>
                                                                <input type="text" class="form-control" placeholder="Enter Name">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="mb-2 d-block">
                                                                    Signature & Date
                                                                </label>
                                                                <input type="file" class="form-control" placeholder="Enter Name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end name & signature field -->

                                                </div>

                                            </div>
                                            <!--  end row -->



                                        </div>
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />

                                    </fieldset>
                                    <!-- end schedule 24B  -->

                                    <!-- start schedule 24C  -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">Schedule 24C</h2>
                                            <h6 style="text-align: center">Particulars of income from business or profession
                                            </h6>
                                            <h6 style="text-align: center">Annex this Schedule to the return of income
                                                if you have income from Business or profession</h6>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            01. Assessment Year
                                                        </label>

                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            02. Tin
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Tin">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">

                                                    <table class="table table-bordered">
                                                        <tr>

                                                            <th> 3. Type of main business or profession</th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table class="table table-bordered">
                                                                    <tbody>

                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>4</td>

                                                                            <td>
                                                                                Name(s) of the business or profession: (as in trade licence)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>5</td>
                                                                            <td>
                                                                                Address(es):
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>

                                                <div class="col-lg-12">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col" class="w-50">Summary of income</th>
                                                                <th></th>
                                                                <th scope="col" class="w-25">Amount</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <!-- end single item -->
                                                            <tr>

                                                                <td colspan="3">


                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="w-5">06</td>
                                                                                <td class="w-50">
                                                                                    Sales/ Turnover/Receipts
                                                                                </td>
                                                                                <td class="w-25">
                                                                                    <input type="text" class="form-control">
                                                                                </td>

                                                                            </tr>
                                                                            <!-- end single item -->

                                                                            <tr>
                                                                                <td>07</td>
                                                                                <td>
                                                                                    Gross Profit
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- end single item -->

                                                                            <tr>
                                                                                <td>08</td>
                                                                                <td>
                                                                                    General, administrative, selling and other expenses
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- end single item -->
                                                                            <tr>
                                                                                <td>09</td>
                                                                                <td>
                                                                                    Net Profit(07-08)
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- end single item -->
                                                                        </tbody>
                                                                    </table>

                                                                </td>

                                                            </tr>
                                                            <tr>

                                                                <td colspan="3">


                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th scope="col" class="w-5">Summary of Balance Sheet</th>
                                                                                <th></th>
                                                                                <th scope="col" class="w-25">Amount</th>

                                                                            </tr>
                                                                            <tr>
                                                                                <td class="w-5">10</td>
                                                                                <td class="w-50">
                                                                                    Cash in hand & at bank
                                                                                </td>
                                                                                <td class="w-25">
                                                                                    <input type="text" class="form-control">
                                                                                </td>

                                                                            </tr>
                                                                            <!-- end single item -->

                                                                            <tr>
                                                                                <td>11</td>
                                                                                <td>
                                                                                    Inventories
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- end single item -->

                                                                            <tr>
                                                                                <td>12</td>
                                                                                <td>
                                                                                    Fixed assets
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- end single item -->
                                                                            <tr>
                                                                                <td>13</td>
                                                                                <td>
                                                                                    Other assests
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>14</td>
                                                                                <td>
                                                                                    Total asset
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>


                                                                            <tr>
                                                                                <td>15</td>
                                                                                <td>
                                                                                    Opening capital
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td>16</td>
                                                                                <td>
                                                                                    Net Profit
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>


                                                                            <tr>
                                                                                <td>17</td>
                                                                                <td>
                                                                                    Withdrawals in the income year
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td>18</td>
                                                                                <td>
                                                                                    Closing capital
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td>19</td>
                                                                                <td>
                                                                                    Liabilities
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td>20</td>
                                                                                <td>
                                                                                    Total Capital and liabilities
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" class="form-control">
                                                                                </td>
                                                                            </tr>

                                                                            <!-- end single item -->
                                                                        </tbody>
                                                                    </table>

                                                                </td>

                                                            </tr>
                                                            <!-- end single item -->

                                                            <!-- end single item -->

                                                        </tbody>
                                                    </table>

                                                </div>

                                                <div class="col-lg-12">
                                                    <label> Provide information if income from more than one house property</label>
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th rowspan="4">08</th>
                                                            <td>aggregate of income of all house property (1+2+3=----) (provide additional papaers if necessary)</td>
                                                            <td class="w-25"><input type="text" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="w-25">1</td>
                                                                            <td class="w-50">
                                                                                (Income of from house property 1)
                                                                            </td>
                                                                            <td class="w-25">
                                                                                <input type="text" class="form-control">
                                                                            </td>

                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>2</td>

                                                                            <td>
                                                                                (Income of from house property 2)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>3</td>
                                                                            <td>
                                                                                (Income of from house property 3)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>

                                                    <!-- start name & signature field -->
                                                    <div class=" row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="mb-2 d-block">
                                                                    Name
                                                                </label>
                                                                <input type="text" class="form-control" placeholder="Enter Name">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="mb-2 d-block">
                                                                    Signature & Date
                                                                </label>
                                                                <input type="file" class="form-control" placeholder="Enter Name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end name & signature field -->

                                                </div>

                                            </div>
                                            <!--  end row -->



                                        </div>
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />

                                    </fieldset>
                                    <!-- end schedule 24C  -->

                                    <!-- start schedule 24D  -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">Schedule 24D</h2>
                                            <h6 style="text-align: center">Particulars of Tax credit/rebate
                                            </h6>
                                            <h6 style="text-align: center">to be annexd to return by on assessee claiming investment tax credit<br>
                                                (Attach the proof of claimed investment, contribution, etc)
                                            </h6>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            01. Assessment Year
                                                        </label>

                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            02. Tin
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Tin">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">

                                                    <table class="table table-bordered">
                                                        <tr>

                                                            <th>Particulars of rebatable investment, contribution, etc</th>
                                                            <th> Amount</th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="w-25">03</td>
                                                                            <td class="w-50">
                                                                                Life insurance premium
                                                                            </td>
                                                                            <td class="w-25">
                                                                                <input type="text" class="form-control">
                                                                            </td>

                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>04</td>

                                                                            <td>
                                                                                Contribution to deposit pension scheme (not exceeding allowable limit)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>05</td>
                                                                            <td>
                                                                                Investment in approvedsaving Certificate
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>06</td>
                                                                            <td>
                                                                                Investment in approvedsaving debenture or debenture stock, stock or shares
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>07</td>
                                                                            <td>
                                                                                Contribution to provident fund to which provident Fund Act, 1925 applies
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>08</td>
                                                                            <td>
                                                                                Self contribution and employer's contribution to recognized provident fund
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>09</td>
                                                                            <td>
                                                                                Contribution to Super Annualtion Fund
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>10</td>
                                                                            <td>
                                                                                Contribution to Benevolent Fund and Group insurance Premium
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>11</td>
                                                                            <td>
                                                                                Contribution to Zakat Fund
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>12</td>
                                                                            <td>
                                                                                Others, if any (give details)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>13</td>
                                                                            <td>
                                                                                Total allowable investment, contribution, etc.
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>14</td>
                                                                            <td>
                                                                                Eligible amount for rebate (the lesser of 14A. 14B or 14C)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>14A</td>
                                                                            <td>
                                                                                ....% of the total income [excluding any income for which a tax exemption or a reduced rate is applicable under sub-section (4) of section 44 or any income from any source or sources mentiond in clause (a) of sub-section (2) of section 82c.]
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>14C</td>
                                                                            <td>
                                                                                1.5 crore
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>15</td>
                                                                            <td>
                                                                                Amount of tax rebate calculated on eligible amount (Serial14) under section 44(2)(b)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->
                                                                    </tbody>
                                                                </table>

                                                                <!-- start name & signature field -->
                                                                <div class=" row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label class="mb-2 d-block">
                                                                                Name
                                                                            </label>
                                                                            <input type="text" class="form-control" placeholder="Enter Name">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label class="mb-2 d-block">
                                                                                Signature & Date
                                                                            </label>
                                                                            <input type="file" class="form-control" placeholder="Enter Name">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- end name & signature field -->

                                                            </td>
                                                        </tr>
                                                    </table><!-- end table -->
                                                </div><!-- end col -->
                                            </div><!-- end row -->
                                        </div><!-- end card-form -->
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <!-- end schedule 24D  -->


                                    <!-- Statement of assets, liabilities and expenses -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title" style="text-align: center">
                                                Statement of assets, libabilities and expenses
                                            </h2>
                                            <h6 style="text-align: center">
                                                Under Section 80(1)of the income tax ordinance, 1984 (XXXVI of 1984)
                                            </h6>
                                            <div class="row mb-4">

                                                1. Mention the amount of assets and liabilities that you have at the last date of the income year.
                                                All items shall be at cost value include legal, registration and all other related costs;
                                                <br>

                                                2.if your spouse or minor children and dependensts(s) are not assessee,
                                                you have to include their assests and liabilities in your statement;
                                                <br>

                                                3.Schedule 25 is the integral part of this statement if you have business capital or
                                                agriculture or non-agricuultural property. provide additional papers if neccessary.

                                            </div><!--  end row -->

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            01. Assessment Year
                                                        </label>

                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            02. Statement as on (DD-MM-YYYY)
                                                        </label>

                                                        <div class="multiple-input-box d-flex ">
                                                            <div class="part1 d-flex mr-3">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="3">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="0">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-3" placeholder="6">

                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                                                <input type="text" class="form-control box-sm-box mr-1" placeholder="9">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            03. Name of the Assessee
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Tin">
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="mb-2 d-block">
                                                            04. Tin
                                                        </label>
                                                        <input type="text" class="form-control" placeholder="Enter Tin">
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-12">
                                                    <table class="table table-bordered">

                                                        <!-- table header data first row -->
                                                        <tr>
                                                            <th colspan="2">Particulars</th>
                                                            <th> Amount</th>
                                                        </tr>
                                                        <!-- end table header data first row -->
                                                        <tr>
                                                            <td colspan="3">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <th rowspan="4" colspan="3">05</th>
                                                                                <th>Business Capital (05A+05B)</th>

                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <table class="table table-bordered">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="w-25">05-A</td>
                                                                                                <td class="w-50">
                                                                                                    Business Capital Other than 05 B
                                                                                                </td>
                                                                                                <td class="w-25">
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>

                                                                                            </tr>
                                                                                            <!-- end inner  single item -->

                                                                                            <tr>
                                                                                                <td>05-B</td>
                                                                                                <td>
                                                                                                    Director's Shareholdings in limited companies (as in schedule 25)
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- end inner table  with single item-->

                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <th rowspan="4" colspan="3">06</th>
                                                                                <th>Non-agricultural property</th>
                                                                                <!-- <td class="w-25"><input type="text" class="form-control"></td> -->
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <table class="table table-bordered">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="w-25">06-A</td>
                                                                                                <td class="w-50">
                                                                                                    Non-agricultural property (as in schedule 25)
                                                                                                </td>
                                                                                                <td class="w-25">
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>

                                                                                            </tr>
                                                                                            <!-- end inner  single item -->

                                                                                            <tr>
                                                                                                <td>06-B</td>
                                                                                                <td>
                                                                                                    Advance made for non-agricultural property (as in schedule 25)
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                            <td>07</td>
                                                                            <td>
                                                                                Agricultural property (as in schedule25)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->
                                                                        </table>
                                                                        <!-- end inner table  with single item-->

                                                                        

                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <th rowspan="4" colspan="3">08</th>
                                                                                <th>Financial assets value (08A+08B+08C+08D+08E)</th>

                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <table class="table table-bordered">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="w-25">08-A</td>
                                                                                                <td class="w-50">
                                                                                                    Share, debentures etc.
                                                                                                </td>
                                                                                                <td class="w-25">
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner  single item -->

                                                                                            <tr>
                                                                                                <td>08-B</td>
                                                                                                <td>
                                                                                                    Life Insurance Premium
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                            <tr>
                                                                                                <td>08-C</td>
                                                                                                <td>
                                                                                                    Fixed deposit, Term deposits and DPS
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                            <tr>
                                                                                                <td>08-D</td>
                                                                                                <td>
                                                                                                    Loans given to others (Mention name and tin)
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                            <tr>
                                                                                                <td>08-E</td>
                                                                                                <td>
                                                                                                    Other financial assets(give deatils)
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- end inner table  with single item-->

                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <th rowspan="4">09</th>
                                                                                <td>Motro Cars (use additional papers if more than two cars)</td>
                                                                                <td class="w-25"><input type="text" class="form-control"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <table class="table table-bordered">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td>Sl.</td>
                                                                                                <td>
                                                                                                    Brand Name
                                                                                                </td>
                                                                                                <td>
                                                                                                    Engine (CC)
                                                                                                </td>
                                                                                                <td>
                                                                                                    Registration No
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner  single item -->

                                                                                            <tr>
                                                                                                <td>01</td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                            <tr>
                                                                                                <td>02</td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- end inner table  with single item-->

                                                                        <tr>
                                                                            <td>10</td>
                                                                            <td>
                                                                                Gold, diamond, gems, and otehr items (mention quantity)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>11</td>
                                                                            <td>
                                                                                Furniture, equipment and electronics items
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <tr>
                                                                            <td>12</td>
                                                                            <td>
                                                                                Other assets of significant value (jewellery)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <th rowspan="4">13</th>
                                                                                <td>Cash and fund outside business (13A+13B+13C+13D)</td>
                                                                                <td class="w-25"><input type="text" class="form-control"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <table class="table table-bordered">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="w-25">13-A</td>
                                                                                                <td class="w-50">
                                                                                                    Notes and currencies
                                                                                                </td>
                                                                                                <td class="w-25">
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner  single item -->

                                                                                            <tr>
                                                                                                <td>13-B</td>
                                                                                                <td>
                                                                                                    Banks, Cards and electronic cash
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                            <tr>
                                                                                                <td>13-C</td>
                                                                                                <td>
                                                                                                    Provident fund and ohter fund
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                            <tr>
                                                                                                <td>13-D</td>
                                                                                                <td>
                                                                                                    Other deposits, balance and advance(other than 8)
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" class="form-control">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- end inner single item -->

                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- end inner table  with single item-->

                                                                        <tr>
                                                                            <td>14</td>
                                                                            <td>
                                                                               Gross wealth (aggregate of 05 to 13)
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control">
                                                                            </td>
                                                                        </tr>
                                                                        <!-- end single item -->

                                                                    </tbody>
                                                                </table>
                                                                <!-- end inner table table -->

                                                                <!-- start name name & signature field -->
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label class="mb-2 d-block">
                                                                                Name
                                                                            </label>
                                                                            <input type="text" class="form-control" placeholder="Enter Name">
                                                                        </div>
                                                                    </div><!-- end col -->

                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label class="mb-2 d-block">
                                                                                Signature & Date
                                                                            </label>
                                                                            <input type="file" class="form-control" placeholder="Enter Name">
                                                                        </div>
                                                                    </div><!-- end col -->
                                                                </div>
                                                                <!-- end name name & signature field -->

                                                            </td>
                                                        </tr>
                                                    </table><!-- end table -->
                                                </div><!-- end col -->
                                            </div><!-- end row -->
                                        </div><!-- end from-card -->

                                        <!-- start step button -->
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                        <!-- end step button -->

                                    </fieldset>

                                    <!-- start successfull message -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title text-center">Success !</h2> <br><br>
                                            <div class="row justify-content-center">
                                                <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                            </div> <br><br>
                                            <div class="row justify-content-center">
                                                <div class="col-7 text-center">
                                                    <h5>You Have Successfully Signed Up</h5>
                                                </div>
                                            </div>
                                        </div><!-- end form-card -->
                                    </fieldset>
                                    <!-- end successfull message -->

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->
                <section class="col-lg-5 connectedSortable">
            </div>
        </div>
    </section>
    <!-- right col -->
</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
</section>



<script>
    $(document).ready(function() {

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;

        $(".next").click(function() {

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({
                        'opacity': opacity
                    });
                },
                duration: 600
            });
        });

        $(".previous").click(function() {

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();

            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({
                        'opacity': opacity
                    });
                },
                duration: 600
            });
        });

        $('.radio-group .radio').click(function() {
            $(this).parent().find('.radio').removeClass('selected');
            $(this).addClass('selected');
        });

        $(".submit").click(function() {
            return false;
        })

    });
</script>

@endsection