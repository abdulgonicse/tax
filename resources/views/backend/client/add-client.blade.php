@extends('backend.layouts.master')
@section('content')
<style>
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    height: 100%
  }

  body {
    margin: 0;
    padding: 0;
    font-size: 16px;
    line-height: 26px;

  }

  ul {
    margin: 0;
    padding: 0;
    list-style: none;
  }

  #grad1 {
    background-color: #9C27B0;
    /* background-image: linear-gradient(120deg, #FF4081, #81D4FA) */
  }

  #gothi-form {
    text-align: center;
    position: relative;
    margin-top: 20px;
  }

  #gothi-form fieldset .form-card {
    background: white;
    border: 0 none;
    border-radius: 8px;
    box-shadow: 0 5px 15px rgba(0, 0, 0, .1);
    box-sizing: border-box;
    position: relative;
    padding: 20px;
    margin: 20px;
  }

  #gothi-form fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
  }

  #gothi-form fieldset:not(:first-of-type) {
    display: none
  }

  #gothi-form fieldset .form-card {
    text-align: left;
    /* color: #9E9E9E */
    font-size: 16px;
    line-height: 26px;

  }

  #gothi-form .action-button {
    width: 100px;
    background: skyblue;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
  }

  #gothi-form .action-button:hover,
  #gothi-form .action-button:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px skyblue
  }

  #gothi-form .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
  }

  #gothi-form .action-button-previous:hover,
  #gothi-form .action-button-previous:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
  }

  select.list-dt {
    border: none;
    outline: 0;
    border-bottom: 1px solid #ccc;
    padding: 2px 5px 3px 5px;
    margin: 2px
  }

  select.list-dt:focus {
    border-bottom: 2px solid skyblue
  }

  .card {
    z-index: 0;
    border: none;
    border-radius: 0.5rem;
    position: relative
  }

  .form-card-title {
    font-size: 30px;
    color: #2C3E50;
    margin-bottom: 20px;
    font-weight: bold;
    text-align: center;
  }

  #progressbar {
    overflow: hidden;
    color: lightgrey;
    margin: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  #progressbar .active {
    color: #000000
  }

  #progressbar li {
    list-style-type: none;
    font-size: 12px;
    width: 7%;
    float: left;
    position: relative
  }

  #step_1:before,
  #step_2:before,
  #step_3:before,
  #step_4:before,
  #step_5:before,
  #step_6:before,
  #step_7:before,
  #step_8:before,
  #step_8:before,
  #step_9:before,
  #step_10:before,
  #step_11:before,
  #step_12:before,
  #step_13:before {
    font-family: 'Font Awesome 5 Free';
    content: "\f2bb"
  }

  /* #progressbar #step_2:before {
        font-family: FontAwesome;
        content: "\f007"
    }

    #progressbar #payment:before {
        font-family: FontAwesome;
        content: "\f09d"
    }

    #progressbar #confirm:before {
        font-family: FontAwesome;
        content: "\f00c"
    } */

  #progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 18px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
  }

  #progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
  }

  #progressbar li.active:before,
  #progressbar li.active:after {
    background: skyblue
  }

  .radio-group {
    position: relative;
    margin-bottom: 25px
  }

  .radio {
    display: inline-block;
    width: 204;
    height: 104;
    border-radius: 0;
    background: lightblue;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    box-sizing: border-box;
    cursor: pointer;
    margin: 8px 2px
  }

  .radio:hover {
    box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
  }

  .radio.selected {
    box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
  }

  .fit-image {
    width: 100%;
    object-fit: cover
  }

  .box-sm-box {
    width: 40px !important;
    height: 40px !important;
    border: 1px solid #dee2e6 !important;
    display: flex !important;
    align-items: center !important;
    justify-content: center !important;
    text-align: center !important;
    line-height: 40px !important;
  }

  .form-group .form-control:focus::placeholder {
    opacity: 0;
  }

  #gothi-form label {
    margin-bottom: 0;
  }

  /*form-card-additional-list*/
  .form-card-additional-list li {
    margin-bottom: 5px;
  }

  /*table serial number box*/
  .slno-box {
    width: 60px;
  }

  /*profile pircute image*/
  .profile-picture img {
    width: 200px;
    height: 200px;
    object-fit: cover;
  }

  .table {
    margin-bottom: 0;
  }

  @media(max-width: 991px) {
    .table-row {
      display: flex;
      flex-wrap: wrap;
    }

    .table-row .td-width_50 {
      width: 100%;
    }
  }

  th {
    text-align: center;
  }
</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row justify-content-center ">
        <div class="col-12  text-center p-0 mt-3 mb-2">
          <div class="card px-0 pt-4 pb-0 mt-2 mb-2">
            <div class="form-heading">
              <h2><strong>Sign Up Your User Account</strong></h2>
              <p><strong>Fill all form field to go to next step</strong></p>
            </div>
            <div class="row">
              <div class="col-md-12 mx-0">
                <form id="gothi-form">
                  @csrf

                  <!-- start progressbar -->
                  <ul id="progressbar">
                    <li class="active" id="step_1"><strong>1</strong></li>
                    <li id="step_2"><strong>2</strong></li>
                    <li id="step_3"><strong>3</strong></li>
                    <li id="step_4"><strong>4</strong></li>
                    <li id="step_5"><strong>5</strong></li>
                    <li id="step_6"><strong>6</strong></li>
                    <li id="step_7"><strong>7</strong></li>
                    <li id="step_8"><strong>8</strong></li>
                    <li id="step_9"><strong>9</strong></li>
                    <li id="step_10"><strong>10</strong></li>
                    <li id="step_11"><strong>11</strong></li>
                    <li id="step_12"><strong>12</strong></li>
                    <li id="step_13"><strong>13</strong></li>
                  </ul>
                  <!-- end progressbar -->

                  <!-- start Individual Assessee == Page No-01  -->
                  <fieldset>
                    <div class="form-card">

                      <!-- start additional text row -->
                      <div class="row justify-content-between justify-cotent-md-center">
                        <div class="col-lg-8 ">
                          <div class="form-card-additional-text p-2">
                            <p>
                              <strong>
                                The following schedules shall be the integral part of this reutrn and must be annexed
                                to reutrn in the following cases :
                              </strong>
                            </p>
                            <ul class="form-card-additional-list">
                              <li>
                                Schedule 24-A : if you have income from salaries .
                              </li>
                              <li>
                                Schedule 24-A : if you have income from house Property .
                              </li>
                              <li>
                                Schedule 24-A : if you have income from business or profession .
                              </li>
                              <li>
                                Schedule 24-A : if you claim tax rebate .
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="col-lg-4 mb-3 text-lg-right">
                          <div class="profile-picture">
                            <img src="" alt="">
                          </div>
                        </div>
                      </div>
                      <!-- end additional text row -->


                      <!-- start form card title -->
                      <div class="mb-3 text-center">
                        <p class="mb-0"><strong>Part 1</strong></p>
                        <h2 class="form-card-title">Basic Information</h2>
                      </div>
                      <!-- start form card title -->

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Return submitted under section 82BB(Select One)
                            </label>

                            <div class="form-check form-check-inline">
                              <input class="form-check-input" value="yes" type="radio" name="return_submitted_yes" id="inlineRadio1" value="option1">
                              <label class="form-check-label" for="inlineRadio1">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" value="no" type="radio" name="return_submitted_no" id="inlineRadio2" value="option2">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              03. Name of the Assessee
                            </label>
                            <input type="text" name="name_of_the_assessee1" class="form-control" placeholder="Enter Name of the Assessee">
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              04. Gender
                            </label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="male" id="male" value="option1">
                              <label class="form-check-label" for="male">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="female" id="female" value="gender">
                              <label class="form-check-label" for="female">Female</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="others" id="others" value="others">
                              <label class="form-check-label" for="others">Others</label>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              05. 12-digit Tin:
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              06. Old Tin:
                            </label>
                            <input type="text" name="old_tin" class="form-control" placeholder="Enter Old Tin">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              07. Circle:
                            </label>
                            <input type="text" name="circle" class="form-control" placeholder="Enter Circle ">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              08. Zone:
                            </label>
                            <input type="text" name="zone" class="form-control" placeholder="Enter Zone" ">
                                                    </div>
                                                </div>
                                                <div class=" col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                09. Resident Status (Tick One)
                              </label>

                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="resident" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">Resident</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="non_resident" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">Non-Resident</label>
                              </div>
                            </div>
                          </div>


                          <div class="col-lg-12">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                11. Tick on the box(es) below if you are
                              </label>

                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="freedom_figher" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">A
                                  Gazetted war-wounded freedom figher</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="disability" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">A person
                                  with disability</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="aged_more" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">Aged 65
                                  years or more</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">A
                                  parent/legal guardian of a person with
                                  disability</label>
                              </div>
                            </div>
                          </div>
                          <!-- end col -->

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                11. Date of Birth (DD-MM-YYYY)
                              </label>
                              <div class="multiple-input-box d-flex ">
                                <div class="part1 d-flex mr-3">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="5">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="4">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="9">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="7">
                                  <input type="text" class="form-control box-sm-box" placeholder="2">
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- end col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                12. Income Year
                              </label>
                              <div class="income-year-box d-flex">
                                <input type="date" name="start_date">
                                <span class="mx-3 font-bold">to</span>
                                <input type="date" name="end_date">
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                13. If Employed
                              </label>
                              <input type="text" class="form-control" name="if_employed" placeholder="Enter If Employed">
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                14. Spouse Name:
                              </label>
                              <input type="text" class="form-control" name="spouse_Name" placeholder="Enter Spouse Name">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                15. Spouse TIN (if any)
                              </label>
                              <input type="text" class="form-control" name="spouse_Tin" placeholder="Enter Spouse TIN">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                16. Father's Name:
                              </label>
                              <input type="text" name="father_name" class="form-control" placeholder="Enter Father's Name">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                17. Mother's Name:
                              </label>
                              <input type="text" name="mather_name" class="form-control" placeholder="Enter Mother's Name">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                18. Present Address:
                              </label>
                              <input type="text" name="present_address" class="form-control" placeholder="Enter Present Address">
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                19. Permanent Address:
                              </label>
                              <input type="text" name="permanent_address" class="form-control" placeholder="Enter Permanent Address">
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                20. Contact Telephone:
                              </label>
                              <input type="number" name="contact_telephone" class="form-control" placeholder="Enter Contact Telephone">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                21. E-Mail:
                              </label>
                              <input type="email" name="email" class="form-control" placeholder="Enter E-Mail">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                22. National Identification Number:
                              </label>
                              <input type="number" name="nid" class="form-control" placeholder="Enter NID">
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                23. Business Identification Number(s):
                              </label>
                              <input type="text" name="business_identification_number" class="form-control" placeholder="Enter Business Identification Number">
                            </div>
                          </div>
                          <!-- end col -->

                        </div>
                        <!--  end row -->



                      </div>
                      <input type="button" name="next" class="next action-button" value="Next Step" />

                  </fieldset>
                  <!-- end Individual Assessee == Page No-01 -->

                  <!-- start particulars of income tax  -->
                  <fieldset>
                    <div class="form-card">

                      <div class="row justify-content-center mb-4">
                        <div class="col-lg-8 text-center mx-auto">
                          <h2 class="form-card-title">
                            Particulars Of Income and Tax
                          </h2>
                          <div class="form-group">
                            <div class="multiple-input-box d-flex align-items-center justify-content-center">
                              <div class="mr-3">
                                <label>Tin:</label>
                              </div>
                              <div class="part1 d-flex ">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="5">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="4">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="9">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="7">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box" placeholder="2">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                24. Salaies (annex Schedule 24A)
                              </label>
                            </div>
                            <div class="col-lg-3">
                              S.21
                            </div>

                            <div class="col-lg-3">
                              <input type="number" name="salaies_annex_schedule_24a" class="form-control test income-input" placeholder="Enter Salaies">
                            </div>

                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                25. Interest on securities
                              </label>
                            </div>
                            <div class="col-lg-3">
                              S.22
                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="interest_on_securities" class="form-control test income-input" placeholder="Enter Interest on securities">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                26. Income from house property(annex Schedule 24B)
                              </label>
                            </div>
                            <div class="col-lg-3">
                              S.24
                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="income_from_house_property" class="form-control income-input" placeholder="Enter Income from house property">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                27. Agricultural income
                              </label>
                            </div>
                            <div class="col-lg-3">
                              S.26
                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="agricultural_income" class="form-control income-input " placeholder="Enter Agricultural income">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                28. Income from business or profession (annex Schedule
                                24c)
                              </label>
                            </div>
                            <div class="col-lg-3">
                              S.28
                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="income_from_business_or_profession" class="form-control income-input" placeholder="Enter Income from business or profession">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                29. Capital gains
                              </label>
                            </div>
                            <div class="col-lg-3">
                              S.31
                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="capital_gains" class="form-control income-input" placeholder="Enter Capital gains">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                30. Income from other sources
                              </label>
                            </div>
                            <div class="col-lg-3">
                              S.33
                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="income_from_other_sources" class="form-control income-input" placeholder="Enter Income from other sources">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                31. Share of income from firm or AOP
                              </label>
                            </div>
                            <div class="col-lg-3">

                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="share_of_income_from_firm_or_aop" class="form-control income-input" placeholder="Enter Share of income from firm or AOP">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                32. Income of minor or spouse under section 43(4)
                              </label>
                            </div>
                            <div class="col-lg-3">
                              S.43
                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="income_of_minor_or_spouse income-input" class="form-control " placeholder="Enter Income of minor or spouse under section">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                33. Foreign income
                              </label>
                            </div>
                            <div class="col-lg-3">

                            </div>
                            <div class="col-lg-3">
                              <input type="number" name="foreign_income" class="form-control income-input" placeholder="Enter Foreign income">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between total_incomes">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                34. Total income (aggregate of 24 to 33)
                              </label>
                            </div>
                            <div class="col-lg-3">
                            </div>
                            <div class="col-lg-3">

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-card">
                      <h2 class="form-card-title" style="text-align: center">Tax Computation and Payment
                      </h2>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                35. Gross tax before tax rebate
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" class="form-control " placeholder="Enter Gross tax before tax rebate" name="gross_tax_before_tax_rebate">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                36. Tax rebate (annex Schedule 24D)
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" class="form-control" name="tax_rebate" placeholder="Enter Tax rebate">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                37. Net tax after tax rebate
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" class="form-control" name="net_tax_after_tax_rebate" placeholder="Enter Net tax after tax rebate">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                38. Minimum tax
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" class="form-control" name="minimum_tax" placeholder="Enter Minimum tax">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                39. Net wealth surchange
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" class="form-control" name="net_wealth_surchange" placeholder="Enter Net wealth surchange">
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                40. Interest or any other amount under the Ordinance (if
                                any)
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" class="form-control" name="interest_or_any_other" placeholder="Enter Interest or any other amount under the Ordinance">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                41. Total amount payable
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" class="form-control total_amount_payable" name="total_amount_payable" placeholder="Enter Total amount payable">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                42. Tax deducted or collected at source (attach proof)
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" name="tax_deducted" class="form-control adjust_tax" placeholder="Enter Tax deducted">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                43. Advance tax paid (attach proof)
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" name="advance_tax_paid" class="form-control adjust_tax" placeholder="Enter Advance tax paid">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                44. Adjustment of tax refund [mention assessment year(s)
                                of refund]
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" class="form-control adjust_tax" name="adjustment_of_tax" placeholder="Enter Adjustment of tax refund">
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                45. Amount paid with return (attach proof) U/S 74
                                (Challan No. Dated: 11/2019, Mohakhali Branch, Sonali
                                Bank Limited)
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" name="amount_paid" class="form-control adjust_tax" placeholder="Enter Amount paid with return">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                46. Total amount paid and adjusted (42+43+44+45)
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <span class="total_adjust_tax"></span>
                              {{-- <input type="number" name="total_adjust_tax" class="form-control total_adjust_tax" placeholder="Enter Total amount paid"> --}}
                              <!-- <input type="number" name="total_amount" class="form-control total_adjust_tax" placeholder="Enter Total amount paid"> -->

                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                47. Deficit or excess (refunddable)(41-46)
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <span class="deficit_or_excess_refunddable"></span>
                              {{-- <input type="number" name="deficit_or_excess_refunddable" class="form-control deficit_or_excess_refunddable" placeholder="Enter Deficit or excess"> --}}
                              <!-- <input type="number" name="deficit_or_excess_refunddable" class="form-control " placeholder="Enter Deficit or excess"> -->
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-9">
                              <label class="mb-2">
                                48. Tax exempted income
                              </label>
                            </div>

                            <div class="col-lg-3">
                              <input type="number" name="tax_exempted" class="form-control " placeholder="Enter Tax exempted income">
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                  </fieldset>
                  <!-- end particulars of income tax  -->

                  <!-- start instuction , enclosure and verification  -->
                  <fieldset>
                    <div class="form-card">
                      <div class="col-lg-12">
                        <div class="form-group">
                          <div class="multiple-input-box d-flex align-items-center justify-content-center">
                            <div class="mr-3">
                              <label>Tin:</label>
                            </div>
                            <div class="part1 d-flex ">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="5">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="4">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="9">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="7">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                              <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                              <input type="text" class="form-control box-sm-box" placeholder="2">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-12">
                              <label>
                                49. Instructions <br />

                                (1). Statement of assets, liabilities and expenses
                                (IT-10B2016) and statement of life style expense (IT-
                                10BB2016) must be furnished with the return unless you
                                are exempted from furnishing such statement(s) under
                                section 80.
                                <br />(2). Proof of payments of tax, including Advance tax
                                and withholding tax and the proof of investment for tax
                                rebate must be provided along with return. <br />
                                (3). Attach account statements and other documents where
                                applicable
                              </label>
                            </div>


                          </div>
                        </div>
                      </div>


                      <div class="col-lg-12">
                        <div class="form-group">

                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                50. If your a parent of a person with disability, has
                                your spouse availed the extended tax exemption
                                threshold?(tick one)
                              </label>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="If_your_a_parent_yes" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="If_your_a_parent_no" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">No</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">

                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                51. Are you required to submit a statement of assets,
                                liabilities and expenses (IT-10b2016) under section
                                80(1) ? (tick one)
                              </label>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="are_you_required_yes" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="are_you_required_no" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">No</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-lg-12">
                        <div class="form-group">

                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-12">
                              <label class="mb-2">
                                52. Schedules annexed (tick all that are applicable)
                              </label>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="schedules_annexed_24a" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">24A</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="schedules_annexed_24b" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">24B</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="schedules_annexed_24c" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">24C</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="schedules_annexed_24c" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">24D</label>
                              </div>
                            </div>


                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">

                          <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6">
                              <label class="mb-2">
                                53. Statement annexed (tick all that are applicable)
                              </label>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">IT-10B2016</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">IT-10BB2016</label>
                              </div>

                            </div>


                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <label class="mb-3 d-block">
                            54. Other Statement, documents, etc. attached (list all)
                          </label>

                          <div class="row align-items-center">
                            <div class="col-lg-6 d-flex align-items-center">
                              <label class="mr-2">(1). Challan No</label>
                              <input type="text" class="form-control w-50" name="challan_no" placeholder="Enter Challan No">
                            </div>
                            <div class="col-lg-6 d-flex align-items-center">
                              <label class="mr-2">Dated - </label>
                              <select class="list-dt" id="month" name="day">
                                <option selected>Day</option>
                                <option>02</option>
                                <option>03</option>
                                <option>04</option>
                                <option>05</option>
                                <option>06</option>
                                <option>07</option>
                                <option>08</option>
                                <option>09</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                                <option>13</option>
                              </select>
                              <select class="list-dt" id="month" name="month">
                                <option selected>Month</option>
                                <option>January</option>
                                <option>February</option>
                                <option>March</option>
                                <option>April</option>
                                <option>May</option>
                                <option>June</option>
                                <option>July</option>
                                <option>August</option>
                                <option>September</option>
                                <option>October</option>
                                <option>November</option>
                                <option>December</option>
                              </select>

                              <select class="list-dt" id="year" name="year">
                                <option selected>Year</option>
                                <option>2010</option>
                                <option>2010</option>
                                <option>2010</option>
                                <option>2010</option>
                                <option>2010</option>
                                <option>2010</option>

                              </select>
                            </div>
                            <div class="col-lg-4 my-3 d-flex align-items-center">
                              <label class="mr-2">(2). Income Tax Computation Sheet</label>
                              <input type="text" name="income_tax_computation_sheet" class="form-control w-50">
                            </div>
                            <div class="col-lg-4 my-3 d-flex align-items-center">
                              <label class="mr-2">(3). Salary Certificate</label>
                              <input type="text" name="salary_certificate" class="form-control w-50">
                            </div>
                            <div class="col-lg-4 my-3 d-flex align-items-center">
                              <label class="mr-2">(4). Bank statement</label>
                              <input type="text" name="bank_statement" class="form-control w-50">

                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <label class="mb-2 d-block">
                            55. Verification I solemnly declare that to the best of my
                            knowledge and belief the information given in this retrn and
                            statements and documents annexed or attached here with are
                            correct and complete.
                          </label>

                          <div class="row align-items-center">
                            <div class="col-lg-6 d-flex align-items-center">
                              <label class="mr-2">Name:</label>
                              <input type="text" name="name" class="form-control w-50" placeholder="Enter  Name">
                            </div>
                            <div class="col-lg-6 d-flex align-items-center">
                              <label class="mr-2">Place of signature</label>
                              <input type="text" name="signature_place" class="form-control w-50">
                            </div>
                            <br><br>
                            <div class="col-lg-8 d-flex align-items-center">
                              <label class="mr-2">Date of singnature (DD-MM-YY)</label>
                              <div class="multiple-input-box d-flex ">
                                <div class="part1 d-flex mr-3">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-3" placeholder="0">

                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                  <input type="text" class="form-control box-sm-box mr-3" placeholder="2">

                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-0" placeholder="1">


                                </div>
                              </div>
                            </div>
                            <!-- <div class="col-lg-6 d-flex align-items-center">
                                                            <label class="mr-2">Place of singnature</label>
                                                            <input type="file" class="form-control w-50" placeholder="Enter signature">
                                                        </div> -->

                          </div>
                        </div>
                      </div>


                      <div class="col-lg-12" style="text-align: center">
                        <h3> For the official use only</h3>
                        <h5>Return Submission Information</h5>
                        <div class="form-group">
                          <label class="mb-2 d-block">

                          </label>

                          <div class="row align-items-center">

                            <div class="col-lg-7 d-flex align-items-center">
                              <label class="mr-2">Date of singnature (DD-MM-YY)</label>
                              <div class="multiple-input-box d-flex ">
                                <div class="part1 d-flex mr-3">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-3" placeholder="0">

                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                  <input type="text" class="form-control box-sm-box mr-3" placeholder="2">

                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-0" placeholder="1">


                                </div>
                              </div>
                            </div>
                            <div class="col-lg-5 d-flex align-items-center">
                              <label class="mr-2">Tax Office Entry Number</label>
                              <input type="number" name="tax_Office_entry_number" class="form-control w-50" placeholder="Enter Tax Office Entry Number">
                            </div>

                          </div>
                        </div>
                      </div>


                      <!-- end col -->
                    </div>
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Next Step" />
                  </fieldset>
                  <!-- start instuction , enclosure and verification  -->

                  <!-- start acknowledgement receipt of return of income  -->
                  <fieldset>
                    <div class="form-card">
                      <h2 class="form-card-title" style="text-align: center">Basic Information</h2>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Return submitted under section 82BB(Select One)
                            </label>

                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="return_submitted_yes" id="inlineRadio1" value="option1">
                              <label class="form-check-label" for="inlineRadio1">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="return_submitted_no" id="inlineRadio2" value="option2">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              03. Name of the Assessee
                            </label>
                            <input type="text" name="name_of_the_assessee" class="form-control" placeholder="Enter Name of the Assessee">
                          </div>
                        </div>
                        <!-- end col -->


                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              04. 12-digit Tin:
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Amount">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              05. Old Tin:
                            </label>
                            <input type="text" name="old_tin" class="form-control" placeholder="Enter Amount">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              07. Circle:
                            </label>
                            <input type="text" name="circle" class="form-control" placeholder="Enter Circle" ">
                                                    </div>
                                                </div>
                                                <div class=" col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                08. Tax Zone:
                              </label>
                              <input type="text" name="zone" class="form-control" placeholder="Enter Zone">
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                Amount Payable (Serial 34)
                              </label>
                              <input type="text" name="amount_payable_serial_34" class="form-control" placeholder="Enter Amount Payable 34">
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                Amount Payable (Serial 41)
                              </label>
                              <input type="text" name="amount_payable_serial_41" class="form-control" placeholder="Enter Amount">
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                Amount Payable (Serial 46)
                              </label>
                              <input type="text" name="amount_paid_46" class="form-control" placeholder="Enter Amount Payable 46">
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                Amount of net wealth shown in IT 10B2016
                              </label>
                              <input type="text" name="amount_of_net_wealth_shown" class="form-control" placeholder="Enter Amount of net wealth shown in IT 10B2016">
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                Amount of net wealth surcharge paid
                              </label>
                              <input type="text" name="amount_of_net_wealth_surcharge_paid" class="form-control" placeholder="Enter Amount of net wealth surcharge paid">
                            </div>
                          </div>
                          <div class="col-lg-12">

                            <div class="form-group">
                              <label class="mb-2 d-block">

                              </label>

                              <div class="row align-items-center">

                                <div class="col-lg-7 d-flex align-items-center">
                                  <label class="mr-2">Date of Submisssion
                                    (DD-MM-YY)</label>
                                  <div class="multiple-input-box d-flex ">
                                    <div class="part1 d-flex mr-3">
                                      <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                      <input type="text" class="form-control box-sm-box mr-3" placeholder="0">

                                      <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                      <input type="text" class="form-control box-sm-box mr-3" placeholder="2">

                                      <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                      <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                      <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                      <input type="text" class="form-control box-sm-box mr-0" placeholder="1">


                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-5 d-flex align-items-center">
                                  <label class="mr-2">Tax Office Entry Number</label>
                                  <input type="number" name="taxt_office_entry_number" class="form-control w-50" placeholder="Enter Tax Office Entry Number">
                                </div>

                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                singnature and seal of the offical receiving the return
                              </label>

                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                Date of signature
                              </label>
                              <div class="multiple-input-box d-flex ">
                                <div class="part1 d-flex mr-3">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-3" placeholder="0">

                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                  <input type="text" class="form-control box-sm-box mr-3" placeholder="2">

                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                  <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                  <input type="text" class="form-control box-sm-box mr-0" placeholder="1">


                                </div>
                              </div>

                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="mb-2 d-block">
                                Contact Number of Tax Office
                              </label>
                              <input type="text" name="contact_number_of_tax_Office" class="form-control" placeholder="Enter Contact Number of Tax Office">
                            </div>
                          </div>

                          <!-- end col -->

                        </div>
                        <!--  end row -->



                      </div>
                      <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                  </fieldset>
                  <!-- end acknowledgement receipt of return of income  -->

                  <!-- start schedule 24A  -->
                  <fieldset>
                    <div class="form-card">
                      <h2 class="form-card-title" style="text-align: center">Schedule 24A</h2>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Tin
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-12">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Sl No</th>
                                <th scope="col">Particulars</th>
                                <th scope="col">Amount<br>(A)</th>
                                <th scope="col">Tax Exempted<br>(B)</th>
                                <th scope="col">Taxable<br>(C = A-B)</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>03 </td>
                                <td class="w-50">
                                  Basic Pay
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>
                              <!-- end single item -->

                              <tr>
                                <td>04 </td>
                                <td class="w-50">
                                  Special Pay
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>
                              <!-- end single item -->

                              <tr>
                                <td>05 </td>
                                <td class="w-50">
                                  Arrear Pay (if not included in taxable income
                                  earlier)
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>
                              <!-- end single item -->

                              <tr>
                                <td>06 </td>
                                <td class="w-50">
                                  Dearness allowance
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>
                              <!-- end single item -->

                              <tr>
                                <td>07 </td>
                                <td class="w-50">
                                  House rent allowance
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>
                              <tr>
                                <td>08 </td>
                                <td class="w-50">
                                  Medical allowance
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>
                              <tr>
                                <td>09 </td>
                                <td class="w-50">
                                  Conveyance allowance
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>10 </td>
                                <td class="w-50">
                                  Festival Allowance
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>11 </td>
                                <td class="w-50">
                                  Allowance for support staff
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>12 </td>
                                <td class="w-50">
                                  Leave allowance
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>13 </td>
                                <td class="w-50">
                                  Honorarium/Reward/Fee
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>14 </td>
                                <td class="w-50">
                                  Overtime Allowance
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>15 </td>
                                <td class="w-50">
                                  Bonus / Ex-gratia
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>16 </td>
                                <td class="w-50">
                                  Other Allowances
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>
                              <tr>
                                <td>17 </td>
                                <td class="w-50">
                                  Employer's contribution to a recognized provident
                                  fund
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>18 </td>
                                <td class="w-50">
                                  Interest accrued on a recognized provident provident
                                  fund
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>

                              <tr>
                                <td>19 </td>
                                <td class="w-50">
                                  Deemed income income for transort facility
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>


                              <tr>
                                <td>20 </td>
                                <td class="w-50">
                                  Deemed income for free furnished/unfurnished
                                  accommodation
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>



                              <tr>
                                <td>21 </td>
                                <td class="w-50">
                                  Other, if any (give detail)
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control AmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control taxt_exemptedS24A">
                                </td>
                                <td>
                                  <span class="taxableS24A"></span>
                                  {{-- <input type="number" name="taxable" class="form-control taxableS24A"> --}}
                                </td>
                              </tr>


                              <tr>
                                <td>22 </td>
                                <td class="w-50">
                                  Total
                                </td>
                                <td>
                                  <input type="text" name="amount" class="form-control TotalAmountS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxt_exempted" class="form-control Total_taxt_exemptedS24A">
                                </td>
                                <td>
                                  <input type="text" name="taxable" class="form-control TotaltaxableS24A">
                                </td>
                              </tr>

                              <tr>
                                <td> </td>
                                <td class="w-50">
                                  All figures of amount are in taka ()
                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                              </tr>



                              <!-- end single item -->

                            </tbody>
                          </table>

                          <!-- start name & signature field -->
                          <div class="row">
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="mb-2 d-block">
                                  Name
                                </label>
                                <input type="text" class="form-control" placeholder="Enter Name">
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="mb-2 d-block">
                                  Signature & Date
                                </label>
                                <input type="file" class="form-control" placeholder="Enter Name">
                              </div>
                            </div>
                          </div>
                          <!-- start name & signature field -->

                        </div>
                      </div>
                      <!--  end row -->

                    </div>
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />

                  </fieldset>
                  <!-- end schedule 24A  -->

                  <!-- start schedule 24B  -->
                  <fieldset>
                    <div class="form-card">
                      <h2 class="form-card-title" style="text-align: center">Schedule 24B</h2>
                      <h6 style="text-align: center">Particulars of income from House Property
                      </h6>
                      <h6 style="text-align: center">Annex this Schedule to the return of income
                        if you have income from House Property</h6>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Tin
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>

                        <div class="col-lg-12">
                          <label> For each House Property</label>
                          <table class="table table-bordered">
                            <tr>
                              <th rowspan="4">03</th>
                              <th>Description of the house property</th>
                            </tr>
                            <tr>
                              <td colspan="3">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <td class="w-25">03-A</td>
                                      <td class="w-50">
                                        Address of the property
                                      </td>
                                      <td class="w-25">
                                        <input type="text" name="address_of_the_property" class="form-control">
                                      </td>

                                    </tr>
                                    <!-- end single item -->

                                    <tr>
                                      <td>03-B</td>

                                      <td>
                                        Total Area
                                      </td>
                                      <td>
                                        <input type="text" name="total_area" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end single item -->

                                    <tr>
                                      <td>03-C</td>
                                      <td>
                                        Share of the asessee(%)
                                      </td>
                                      <td>
                                        <input type="text" name="share_of_the_asessee" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end single item -->
                                  </tbody>
                                </table>
                              </td>
                            </tr>

                          </table>
                        </div>

                        <div class="col-lg-12">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col" class="w-10">Sl No</th>
                                <th scope="col " class="w-65">Content</th>
                                <th scope="col" class="w-25">Amount</th>

                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>04 </td>
                                <td>
                                  Annual Value
                                </td>
                                <td>
                                  <input type="text" name="annual_value" class="form-control">
                                </td>
                              </tr>
                              <!-- end single item -->
                              <tr>
                                <td>05 </td>
                                <td colspan="3">
                                  Deducations

                                  <table class="table table-bordered">
                                    <tbody>
                                      <tr>
                                        <td class="w-25">05-A</td>
                                        <td class="w-50">
                                          Repair, collection etc
                                        </td>
                                        <td class="w-25">
                                          <input type="text" name="repair_collection_etc" class="form-control">
                                        </td>

                                      </tr>
                                      <!-- end single item -->

                                      <tr>
                                        <td>035-B</td>
                                        <td>
                                          Municipal or loacal Tax
                                        </td>
                                        <td>
                                          <input type="text" name="municipal_or_loacal_tax" class="form-control">
                                        </td>
                                      </tr>
                                      <!-- end single item -->

                                      <tr>
                                        <td>05-C</td>
                                        <td>
                                          Land Revenue
                                        </td>
                                        <td>
                                          <input type="text" name="land_revenue" class="form-control">
                                        </td>
                                      </tr>
                                      <!-- end single item -->
                                      <tr>
                                        <td>05-D</td>
                                        <td>
                                          Interest on Loan/Mortage/capital charge
                                        </td>
                                        <td>
                                          <input type="text" name="interest_on_loan" class="form-control">
                                        </td>
                                      </tr>
                                      <!-- end single item -->
                                    </tbody>
                                  </table>

                                </td>

                              </tr>
                              <!-- end single item -->
                              <tr>
                                <td>06 </td>
                                <td>
                                  Income from house property(04-05)
                                </td>
                                <td>
                                  <input type="text" name="income_from_house_property" class="form-control">
                                </td>
                              </tr>
                              <!-- end single item -->
                              <tr>
                                <td>07 </td>
                                <td>
                                  Incase of partial ownership, the share of income
                                </td>
                                <td>
                                  <input type="text" name="incase_of_partial" class="form-control">
                                </td>
                              </tr>
                              <!-- end single item -->

                            </tbody>
                          </table>

                        </div>

                        <div class="col-lg-12">
                          <label> Provide information if income from more than one house property</label>
                          <table class="table table-bordered">
                            <tr>
                              <th rowspan="4">08</th>
                              <td>aggregate of income of all house property (1+2+3=----) (provide additional papaers if necessary)</td>
                              <td class="w-25">
                                <input type="text" name="provide_additional" class="form-control">
                              </td>
                            </tr>
                            <tr>
                              <td colspan="3">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <td class="w-25">1</td>
                                      <td class="w-50">
                                        (Income of from house property 1)
                                      </td>
                                      <td class="w-25">
                                        <input type="text" name="income_of_from_house_property_1" class="form-control">
                                      </td>

                                    </tr>
                                    <!-- end single item -->

                                    <tr>
                                      <td>2</td>

                                      <td>
                                        (Income of from house property 2)
                                      </td>
                                      <td>
                                        <input type="text" name="income_of_from_house_property_2" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end single item -->

                                    <tr>
                                      <td>3</td>
                                      <td>
                                        (Income of from house property 3)
                                      </td>
                                      <td>
                                        <input type="text" name="income_of_from_house_property_3" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end single item -->
                                  </tbody>
                                </table>
                              </td>
                            </tr>

                          </table>

                          <!-- start name & signature field -->
                          <div class="row">
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="mb-2 d-block">
                                  Name
                                </label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Name">
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="mb-2 d-block">
                                  Signature & Date
                                </label>
                                <input type="file" class="form-control" placeholder="Enter Name">
                              </div>
                            </div>
                          </div>
                          <!-- end name & signature field -->

                        </div>

                      </div>
                      <!--  end row -->



                    </div>
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />

                  </fieldset>
                  <!-- end schedule 24B  -->

                  <!-- start schedule 24C  -->
                  <fieldset>
                    <div class="form-card">
                      <h2 class="form-card-title" style="text-align: center">Schedule 24C</h2>
                      <h6 style="text-align: center">Particulars of income from business or profession
                      </h6>
                      <h6 style="text-align: center">Annex this Schedule to the return of income
                        if you have income from Business or profession</h6>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Tin
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>

                        <div class="col-lg-12">

                          <table class="table table-bordered">
                            <tr>

                              <th> 3. Type of main business or profession</th>
                            </tr>
                            <tr>
                              <td colspan="3">
                                <table class="table table-bordered">
                                  <tbody>

                                    <!-- end single item -->

                                    <tr>
                                      <td>4</td>

                                      <td>
                                        Name(s) of the business or profession: (as in trade licence)
                                      </td>
                                      <td>
                                        <input type="text" name="trade_licence" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end single item -->

                                    <tr>
                                      <td>5</td>
                                      <td>
                                        Address(es):
                                      </td>
                                      <td>
                                        <input type="text" name="address" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end single item -->
                                  </tbody>
                                </table>
                              </td>
                            </tr>

                          </table>
                        </div>

                        <div class="col-lg-12">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col" class="w-50">Summary of income</th>
                                <th></th>
                                <th scope="col" class="w-25">Amount</th>

                              </tr>
                            </thead>
                            <tbody>

                              <!-- end single item -->
                              <tr>

                                <td colspan="3">


                                  <table class="table table-bordered">
                                    <tbody>
                                      <tr>
                                        <td class="w-5">06</td>
                                        <td class="w-50">
                                          Sales/ Turnover/Receipts
                                        </td>
                                        <td class="w-25">
                                          <input type="text" name="sales_turnover_receipts" class="form-control">
                                        </td>

                                      </tr>
                                      <!-- end single item -->

                                      <tr>
                                        <td>07</td>
                                        <td>
                                          Gross Profit
                                        </td>
                                        <td>
                                          <input type="text" name="gross_profit" class="form-control">
                                        </td>
                                      </tr>
                                      <!-- end single item -->

                                      <tr>
                                        <td>08</td>
                                        <td>
                                          General, administrative, selling and other expenses
                                        </td>
                                        <td>
                                          <input type="text" name="general_administrative" class="form-control">
                                        </td>
                                      </tr>
                                      <!-- end single item -->
                                      <tr>
                                        <td>09</td>
                                        <td>
                                          Net Profit(07-08)
                                        </td>
                                        <td>
                                          <input type="text" name="net_profit" class="form-control">
                                        </td>
                                      </tr>
                                      <!-- end single item -->
                                    </tbody>
                                  </table>

                                </td>

                              </tr>
                              <tr>

                                <td colspan="3">


                                  <table class="table table-bordered">
                                    <tbody>
                                      <tr>
                                        <th scope="col" class="w-5">Summary of Balance Sheet</th>
                                        <th></th>
                                        <th scope="col" class="w-25">Amount</th>

                                      </tr>
                                      <tr>
                                        <td class="w-5">10</td>
                                        <td class="w-50">
                                          Cash in hand & at bank
                                        </td>
                                        <td class="w-25">
                                          <input type="text" name="cash_in_hand_at_bank" class="form-control">
                                        </td>

                                      </tr>
                                      <!-- end single item -->

                                      <tr>
                                        <td>11</td>
                                        <td>
                                          Inventories
                                        </td>
                                        <td>
                                          <input type="text" name="inventories" class="form-control">
                                        </td>
                                      </tr>
                                      <!-- end single item -->

                                      <tr>
                                        <td>12</td>
                                        <td>
                                          Fixed assets
                                        </td>
                                        <td>
                                          <input type="text" name="fixed_assets" class="form-control">
                                        </td>
                                      </tr>
                                      <!-- end single item -->
                                      <tr>
                                        <td>13</td>
                                        <td>
                                          Other assests
                                        </td>
                                        <td>
                                          <input type="text" name="other_assests" class="form-control">
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>14</td>
                                        <td>
                                          Total asset
                                        </td>
                                        <td>
                                          <input type="text" name="total_asset" class="form-control">
                                        </td>
                                      </tr>


                                      <tr>
                                        <td>15</td>
                                        <td>
                                          Opening capital
                                        </td>
                                        <td>
                                          <input type="text" name="opening_capital" class="form-control">
                                        </td>
                                      </tr>

                                      <tr>
                                        <td>16</td>
                                        <td>
                                          Net Profit
                                        </td>
                                        <td>
                                          <input type="text" name="summary_net_profit" class="form-control">
                                        </td>
                                      </tr>


                                      <tr>
                                        <td>17</td>
                                        <td>
                                          Withdrawals in the income year
                                        </td>
                                        <td>
                                          <input type="text" name="withdrawals_in_the_income_year" class="form-control">
                                        </td>
                                      </tr>

                                      <tr>
                                        <td>18</td>
                                        <td>
                                          Closing capital
                                        </td>
                                        <td>
                                          <input type="text" name="closing_capital" class="form-control">
                                        </td>
                                      </tr>

                                      <tr>
                                        <td>19</td>
                                        <td>
                                          Liabilities
                                        </td>
                                        <td>
                                          <input type="text" name="liabilities" class="form-control">
                                        </td>
                                      </tr>

                                      <tr>
                                        <td>20</td>
                                        <td>
                                          Total Capital and liabilities
                                        </td>
                                        <td>
                                          <input type="text" name="total_capital_and_liabilities" class="form-control">
                                        </td>
                                      </tr>

                                      <!-- end single item -->
                                    </tbody>
                                  </table>

                                </td>

                              </tr>
                              <!-- end single item -->

                              <!-- end single item -->

                            </tbody>
                          </table>

                        </div>

                        <div class="col-lg-12">


                          <!-- start name & signature field -->
                          <div class=" row">
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="mb-2 d-block">
                                  Name
                                </label>
                                <input type="text" name="" class="form-control" placeholder="Enter Name">
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="mb-2 d-block">
                                  Signature & Date
                                </label>
                                <input type="file" class="form-control" placeholder="Enter Name">
                              </div>
                            </div>
                          </div>
                          <!-- end name & signature field -->

                        </div>

                      </div>
                      <!--  end row -->



                    </div>
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />

                  </fieldset>
                  <!-- end schedule 24C  -->

                  <!-- start schedule 24D  -->
                  <fieldset>
                    <div class="form-card">
                      <h2 class="form-card-title" style="text-align: center">Schedule 24D</h2>
                      <h6 style="text-align: center">Particulars of Tax credit/rebate
                      </h6>
                      <h6 style="text-align: center">to be annexd to return by on assessee claiming investment tax credit<br>
                        (Attach the proof of claimed investment, contribution, etc)
                      </h6>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Tin
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>

                        <div class="col-lg-12">

                          <table class="table table-bordered">
                            <tr>

                              <th>Particulars of rebatable investment, contribution, etc</th>
                              <th> Amount</th>
                            </tr>
                            <tr>
                              <td colspan="3">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <td class="w-25">03</td>
                                      <td class="w-50">
                                        Life insurance premium
                                      </td>
                                      <td class="w-25">
                                        <input type="text" name="life_insurance_premium" class="form-control">
                                      </td>

                                    </tr>
                                    <!-- end single item -->

                                    <tr>
                                      <td>04</td>

                                      <td>
                                        Contribution to deposit pension scheme (not exceeding allowable limit)
                                      </td>
                                      <td>
                                        <input type="text" name="contribution_to_deposit" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end single item -->

                                    <tr>
                                      <td>05</td>
                                      <td>
                                        Investment in approvedsaving Certificate
                                      </td>
                                      <td>
                                        <input type="text" name="investment_in_approvedsaving_certificate" class="form-control">
                                      </td>
                                    </tr>

                                    <tr>
                                      <td>06</td>
                                      <td>
                                        Investment in approvedsaving debenture or debenture stock, stock or shares
                                      </td>
                                      <td>
                                        <input type="text" name="investment_in_approvedsaving" class="form-control">
                                      </td>
                                    </tr>

                                    <tr>
                                      <td>07</td>
                                      <td>
                                        Contribution to provident fund to which provident Fund Act, 1925 applies
                                      </td>
                                      <td>
                                        <input type="text" name="contribution_to_provident" class="form-control">
                                      </td>
                                    </tr>


                                    <tr>
                                      <td>08</td>
                                      <td>
                                        Self contribution and employer's contribution to recognized provident fund
                                      </td>
                                      <td>
                                        <input type="text" name="self_contribution" class="form-control">
                                      </td>
                                    </tr>

                                    <tr>
                                      <td>09</td>
                                      <td>
                                        Contribution to Super Annualtion Fund
                                      </td>
                                      <td>
                                        <input type="text" name="contribution" class="form-control">
                                      </td>
                                    </tr>

                                    <tr>
                                      <td>10</td>
                                      <td>
                                        Contribution to Benevolent Fund and Group insurance Premium
                                      </td>
                                      <td>
                                        <input type="text" name="contribution_to_benevolent" class="form-control">
                                      </td>
                                    </tr>


                                    <tr>
                                      <td>11</td>
                                      <td>
                                        Contribution to Zakat Fund
                                      </td>
                                      <td>
                                        <input type="text" name="contribution_to_zakat_fund" class="form-control">
                                      </td>
                                    </tr>


                                    <tr>
                                      <td>12</td>
                                      <td>
                                        Others, if any (give details)
                                      </td>
                                      <td>
                                        <input type="text" name="others_if_any_give_details" class="form-control">
                                      </td>
                                    </tr>


                                    <tr>
                                      <td>13</td>
                                      <td>
                                        Total allowable investment, contribution, etc.
                                      </td>
                                      <td>
                                        <input type="text" name="total_allowable_investment" class="form-control">
                                      </td>
                                    </tr>


                                    <tr>
                                      <td>14</td>
                                      <td>
                                        Eligible amount for rebate (the lesser of 14A. 14B or 14C)
                                      </td>
                                      <td>
                                        <input type="text" name="eligible_amount_for_rebate" class="form-control">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>14A</td>
                                      <td>
                                        Total allowable investment, contributin, etc (as in 13)
                                      </td>
                                      <td>
                                        <input type="text" name="total_allowable_investment" class="form-control">
                                      </td>
                                    </tr>

                                    <tr>
                                      <td>14B</td>
                                      <td>
                                        ....% of the total income [excluding any income for which a tax exemption or a reduced rate is applicable under sub-section (4) of section 44 or any income from any source or sources mentiond in clause (a) of sub-section (2) of section 82c.]
                                      </td>
                                      <td>
                                        <input type="text" name="total_income" class="form-control">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>14C</td>
                                      <td>
                                        1.5 crore
                                      </td>
                                      <td>
                                        <input type="text" name="crore" class="form-control">
                                      </td>
                                    </tr>

                                    <tr>
                                      <td>15</td>
                                      <td>
                                        Amount of tax rebate calculated on eligible amount (Serial14) under section 44(2)(b)
                                      </td>
                                      <td>
                                        <input type="text" name="amount_of_tax_rebate" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end single item -->
                                  </tbody>
                                </table>

                                <!-- start name & signature field -->
                                <div class=" row">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="mb-2 d-block">
                                        Name
                                      </label>
                                      <input type="text" name="name" class="form-control" placeholder="Enter Name">
                                    </div>
                                  </div>

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="mb-2 d-block">
                                        Signature & Date
                                      </label>
                                      <input type="file" class="form-control" placeholder="Enter Name">
                                    </div>
                                  </div>
                                </div>
                                <!-- end name & signature field -->

                              </td>
                            </tr>
                          </table><!-- end table -->
                        </div><!-- end col -->
                      </div><!-- end row -->
                    </div><!-- end card-form -->
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                  </fieldset>
                  <!-- end schedule 24D  -->


                  <!-- Statement of assets, liabilities and expenses -->
                  <fieldset>
                    <div class="form-card">
                      <h2 class="form-card-title" style="text-align: center">
                        Statement of assets, libabilities and expenses
                      </h2>
                      <h6 style="text-align: center">
                        Under Section 80(1)of the income tax ordinance, 1984 (XXXVI of 1984)
                      </h6>
                      <div class="row mb-4">

                        1. Mention the amount of assets and liabilities that you have at the last date of the income year.
                        All items shall be at cost value include legal, registration and all other related costs;
                        <br>

                        2.if your spouse or minor children and dependensts(s) are not assessee,
                        you have to include their assests and liabilities in your statement;
                        <br>

                        3.Schedule 25 is the integral part of this statement if you have business capital or
                        agriculture or non-agricuultural property. provide additional papers if neccessary.

                      </div><!--  end row -->

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Statement as on (DD-MM-YYYY)
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="3">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="0">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="6">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="9">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              03. Name of the Assessee
                            </label>
                            <input type="text" name="name_of_the_assessee" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              04. Tin
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-12">
                          <table class="table table-bordered">


                            <tr>
                              <th colspan="2">Particulars</th>
                              <th> Amount</th>
                            </tr>
                            <!-- end table header data first row -->

                            <tr>
                              <td colspan="3">
                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4" colspan="3">05</th>
                                    <th>Business Capital (05A+05B)</th>

                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td class="w-25">05-A</td>
                                            <td class="w-50">
                                              Business Capital Other than 05 B
                                            </td>
                                            <td class="w-25">
                                              <input type="text" name="business_capital_other_than_05_b" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>05-B</td>
                                            <td>
                                              Director's Shareholdings in limited companies (as in schedule 25)
                                            </td>
                                            <td>
                                              <input type="text" name="directors_shareholdings_in_limited_companies" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4" colspan="3">06</th>
                                    <th>Non-agricultural property</th>
                                    <!-- <td class="w-25"><input type="text" class="form-control"></td> -->
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td class="w-25">06-A</td>
                                            <td class="w-50">
                                              Non-agricultural property (as in schedule 25)
                                            </td>
                                            <td class="w-25">
                                              <input type="text" name="non_agricultural_property" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>06-B</td>
                                            <td>
                                              Advance made for non-agricultural property (as in schedule 25)
                                            </td>
                                            <td>
                                              <input type="text" name="advance_made_for_non_agricultural_property" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>07</td>
                                    <td>
                                      Agricultural property (as in schedule25)
                                    </td>
                                    <td>
                                      <input type="text" name="agricultural_property" class="form-control">
                                    </td>
                                  </tr>
                                </table>
                                <!-- end single item -->


                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4" colspan="3">08</th>
                                    <th>Financial assets value (08A+08B+08C+08D+08E)</th>

                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td class="w-25">08-A</td>
                                            <td class="w-50">
                                              Share, debentures etc.
                                            </td>
                                            <td class="w-25">
                                              <input type="text" name="share_debentures_etc" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>08-B</td>
                                            <td>
                                              Life Insurance Premium
                                            </td>
                                            <td>
                                              <input type="text" name="life_insurance_premium" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>08-C</td>
                                            <td>
                                              Fixed deposit, Term deposits and DPS
                                            </td>
                                            <td>
                                              <input type="text" name="fixed_deposit_term_deposits_and_dps" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>08-D</td>
                                            <td>
                                              Loans given to others (Mention name and tin)
                                            </td>
                                            <td>
                                              <input type="text" name="loans_given_to_others_mention_name_and_tin" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>08-E</td>
                                            <td>
                                              Other financial assets(give deatils)
                                            </td>
                                            <td>
                                              <input type="text" name="other_financial_assets_give_deatils" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">09</th>
                                    <td>Motro Cars (use additional papers if more than two cars)</td>
                                    <td class="w-25">
                                      <input type="text" name="motro_cars" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td>Sl.</td>
                                            <td>
                                              Brand Name
                                            </td>
                                            <td>
                                              Engine (CC)
                                            </td>
                                            <td>
                                              Registration No
                                            </td>
                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>01</td>
                                            <td>
                                              <input type="text" name="brand_name" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" name="engine_cc" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" name="registration_no" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>02</td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>10</td>
                                    <td class="w-50">
                                      Gold, diamond, gems, and otehr items (mention quantity)
                                    </td>
                                    <td>
                                      <input type="text" name="mention_quantity" class="form-control">
                                    </td>
                                  </tr>
                                </table>
                                <!-- end single item -->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>11</td>
                                    <td class="w-50">
                                      Furniture, equipment and electronics items
                                    </td>
                                    <td>
                                      <input type="text" name="furniture_equipment_and_electronics_items" class="form-control">
                                    </td>
                                  </tr>
                                </table>
                                <!-- end single item -->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>12</td>
                                    <td class="w-50">
                                      Other assets of significant value (jewellery)
                                    </td>
                                    <td>
                                      <input type="text" name="other_assets_of_significant_value_jewellery" class="form-control">
                                    </td>
                                  </tr>
                                </table>
                                <!-- end single item -->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">13</th>
                                    <td>Cash and fund outside business (13A+13B+13C+13D)</td>
                                    <td class="w-25">
                                      <input type="text" name="cash_and_fund_outside_business_13A_13B_13C_13D" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td class="w-25">13-A</td>
                                            <td class="w-50">
                                              Notes and currencies
                                            </td>
                                            <td class="w-25">
                                              <input type="text" name="notes_and_currencies" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>13-B</td>
                                            <td>
                                              Banks, Cards and electronic cash
                                            </td>
                                            <td>
                                              <input type="text" name="banks_cards_and_electronic_cash" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>13-C</td>
                                            <td>
                                              Provident fund and ohter fund
                                            </td>
                                            <td>
                                              <input type="text" name="provident_fund_and_ohter_fund" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>13-D</td>
                                            <td>
                                              Other deposits, balance and advance(other than 8)
                                            </td>
                                            <td>
                                              <input type="text" name="other_deposits_balance_and_advance_other_than_8" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>14</td>
                                    <td class="w-50">Gross wealth (aggregate of 05 to 13)</td>
                                    <td>
                                      <input type="text" name="gross_wealth_aggregate_of_05_to_13" class="form-control">
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">15</th>
                                    <td>Liabilities outside business (15A+15B+15C)</td>
                                    <td class="w-25">
                                      <input type="text" name="liabilities_outside_business_15A_15B_15C" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td class="w-25">15-A</td>
                                            <td class="w-50">
                                              Borrowdings from banks other finincial instirutions
                                            </td>
                                            <td class="w-25">
                                              <input type="text" name="borrowdings_from_banks_other_finincial_instirutions" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>15-B</td>
                                            <td>
                                              Unsecured loadn (mention name and TIN)
                                            </td>
                                            <td>
                                              <input type="text" name="unsecured_loadn_mention_name_and_tin" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>15-C</td>
                                            <td>
                                              Other loans or overdrafts
                                            </td>
                                            <td>
                                              <input type="text" name="other_loans_or_overdrafts" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>16</td>
                                    <td class="w-50">Net Wealth (14-15)</td>
                                    <td>
                                      <input type="text" name="net_wealth" class="form-control">
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>17</td>
                                    <td class="w-50">Net Wealth at the last date of the pervious income year(previous year pdf adjustment)</td>
                                    <td>
                                      <input type="text" name="net_wealth_at_the_last_date_of_the_pervious_income_year" class="form-control">
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>18</td>
                                    <td class="w-50">Change in net wealth (16-17)</td>
                                    <td><input type="text" name="change_in_net_wealth" class="form-control"></td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">19</th>
                                    <td>Other fund outflow during the income year (19A+19B+19C)</td>
                                    <td class="w-25"><input type="text" name="other_fund_outflow_during_the_income_year_19A_19B_19C" class="form-control"></td>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td class="w-25">19-A</td>
                                            <td class="w-50">
                                              Annual living expenditure and tax payments (as IT-10BB2016)
                                            </td>
                                            <td class="w-25">
                                              <input type="text" name="annual_living_expenditure_and_tax_payments_as_it_10BB2016" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>19-B</td>
                                            <td>
                                              Loss, deductions, expenses, etc. not mentioned in IT-10BB2016
                                            </td>
                                            <td>
                                              <input type="text" name="loss_deductions_expenses_etc_not_mentioned_in_IT_10BB2016" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>19-C</td>
                                            <td>
                                              Gift, donation and contribution
                                            </td>
                                            <td>
                                              <input type="text" name="gift_donation_and_contribution" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>20</td>
                                    <td class="w-50">Total fund outflow in the income year (18+19)</td>
                                    <td><input type="text" name="total_fund_outflow_in_the_income_year_18_19" class="form-control"></td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">21</th>
                                    <td>Sources of fund (21A+21B+21C)</td>
                                    <td class="w-25"><input type="text" name="sources_of_fund_21A_21B_21C" class="form-control"></td>

                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <td class="w-25">21-A</td>
                                            <td class="w-50">
                                              Income shown in the return
                                            </td>
                                            <td class="w-25">
                                              <input type="text" name="income_shown_in_the_return" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>21-B</td>
                                            <td>
                                              Tax exempted income and allowance
                                            </td>
                                            <td>
                                              <input type="text" name="taxexempted_income_and_allowance" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                          <tr>
                                            <td>21-C</td>
                                            <td>
                                              Other receipts (Gift from father & mother)
                                            </td>
                                            <td>
                                              <input type="text" name="other_receipts_gift_from_father_mother" class="form-control">
                                            </td>
                                          </tr>
                                          <!-- end inner single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <td>22</td>
                                    <td class="w-50">Shortage of fund, if any (20-21)</td>
                                    <td><input type="text" name="shortage_of_fund_if_any_2021" class="form-control"></td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <!-- start name name & signature field -->
                                <div class="row">
                                  <div class="col-lg-12 my-2">
                                    <p class="mb-1">
                                      <label>
                                        23. Verification and signature
                                      </label>
                                    </p class="mb-0">

                                    <p>
                                      I solemnly declare that to the best of my knowledge and belief the informatin given in this statement
                                      and the schedule annexed herwith are correct and complete
                                    </p>
                                  </div>

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="mb-2 d-block">
                                        Name
                                      </label>
                                      <input type="text" name="name" class="form-control" placeholder="Enter Name">
                                    </div>
                                  </div><!-- end col -->

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="mb-2 d-block">
                                        Signature & Date
                                      </label>
                                      <input type="file" class="form-control" placeholder="Enter Name">
                                    </div>
                                  </div><!-- end col -->
                                </div>
                                <!-- end name name & signature field -->

                              </td>
                            </tr>
                          </table><!-- end table -->
                        </div><!-- end col -->
                      </div><!-- end row -->
                    </div><!-- end from-card -->

                    <!-- start step button -->
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                    <!-- end step button -->

                  </fieldset>
                  <!-- end Statement of assets, liabilities and expenses -->

                  <!-- schedule 25 -->
                  <fieldset>
                    <div class="form-card">
                      <div class="mb-4">
                        <h2 class="form-card-title" style="text-align: center">
                          SCHEDULE25
                        </h2>
                        <h6 style="text-align: center">
                          to be annexed to the statement of Assets, Liabilities and Expenses (IT-10B2016)
                        </h6>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Tin
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-12">

                          <table class="table table-bordered">
                            <tr>
                              <th rowspan="4">03</th>

                            </tr>
                            <tr>
                              <td colspan="3">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <th>
                                        Sl.
                                      </th>
                                      <th>
                                        Shareholdings in limited companies as director
                                      </th>
                                      <th>
                                        No. of shares
                                      </th>
                                      <th>
                                        Value
                                      </th>
                                    </tr>
                                    <!-- end inner  single item -->

                                    <tr>
                                      <td>01</td>
                                      <td>
                                        <input type="text" name="shareholdings_in_limited_companies_as_director" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="no_of_shares" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="value" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>02</td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>03</td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>04</td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <!-- end inner table  with single item-->

                          <table class="table table-bordered">
                            <tr>
                              <th rowspan="5">04</th>

                            </tr>
                            <tr>
                              <td colspan="3">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <th>Sl.</th>
                                      <th>
                                        Non-agricultural property at cost value or any advance made for such property (description, location and size)
                                      </th>

                                      <th>
                                        Value at the start of income year
                                      </th>
                                      <th>
                                        increased/decreased during the income year
                                      </th>
                                      <th>
                                        value at the last date of income year
                                      </th>
                                    </tr>
                                    <!-- end inner  single item -->

                                    <tr>
                                      <td>01</td>
                                      <td>
                                        <input type="text" name="non_agricultural_property_at_cost_value" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="value_at_the_start_of_income_year" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="increased_decreased_during_the_income_year" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="value_at_the_last_date_of_income_year" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>02</td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>03</td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>04</td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <!-- end inner table  with single item-->

                          <table class="table table-bordered">
                            <tr>
                              <th rowspan="5">05</th>

                            </tr>
                            <tr>
                              <td colspan="3">
                                <table class="table table-bordered">
                                  <tbody>
                                    <tr>
                                      <th>Sl.</th>
                                      <th>
                                        Agricultural property at cost value or any advance made for such property (description, location and size)
                                      </th>

                                      <th>
                                        Value at the start of income year
                                      </th>
                                      <th>
                                        increased/decreased during the income year
                                      </th>
                                      <th>
                                        value at the last date of income year
                                      </th>
                                    </tr>
                                    <!-- end inner  single item -->

                                    <tr>
                                      <td>01</td>
                                      <td>
                                        <input type="text" name="agricultural_property_at_cost_value" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="value_at_the_start_of_income_year" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="increased_decreased_during_the_income_year" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" name="value_at_the_last_date_of_income_year" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>02</td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>03</td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                    <tr>
                                      <td>04</td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control">
                                      </td>
                                    </tr>
                                    <!-- end inner single item -->

                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <!-- end inner table  with single item-->

                          <!-- start name name & signature field -->
                          <div class="row">

                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="mb-2 d-block">
                                  Name
                                </label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Name">
                              </div>
                            </div><!-- end col -->

                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="mb-2 d-block">
                                  Signature & Date
                                </label>
                                <input type="file" class="form-control" placeholder="Enter Name">
                              </div>
                            </div><!-- end col -->
                          </div>
                          <!-- end name name & signature field -->


                        </div><!-- end col -->
                      </div><!-- end row -->
                    </div><!-- end from-card -->

                    <!-- start step button -->
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                    <!-- end step button -->

                  </fieldset>
                  <!-- end schedule 25 -->

                  <!--  Statement of expenses, relating to lifestyle -->
                  <fieldset>
                    <div class="form-card">
                      <div class="mb-4 text-center">
                        <h2 class="form-card-title text-uppercase text-center">
                          Statement of expenses, relating to lifestyle
                        </h2>
                        <h6>
                          Under Section 80(2)of the income tax ordinance, 1984 (XXXVI of 1984)
                        </h6>
                      </div>

                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              01. Assessment Year
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="9">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              02. Statement as on (DD-MM-YYYY)
                            </label>

                            <div class="multiple-input-box d-flex ">
                              <div class="part1 d-flex mr-3">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="3">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="0">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-3" placeholder="6">

                                <input type="text" class="form-control box-sm-box mr-1" placeholder="2">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="0">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="1">
                                <input type="text" class="form-control box-sm-box mr-1" placeholder="9">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              03. Name of the Assessee
                            </label>
                            <input type="text" name="name_of_the_assessee" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="mb-2 d-block">
                              04. Tin
                            </label>
                            <input type="text" name="new_tin" class="form-control" placeholder="Enter Tin">
                          </div>
                        </div>
                        <!-- end col -->

                        <div class="col-lg-12">
                          <table class="table table-bordered">


                            <tr>
                              <th colspan="2">Particulars</th>
                              <th> Amount</th>
                            </tr>
                            <!-- end table header data first row -->

                            <tr>
                              <td colspan="3">

                                <table class="table table-bordered">
                                  <tr>
                                    <th>05</th>
                                    <th class="w-50">
                                      Expenses for food, clothing and other essentials
                                    </th>
                                    <th>
                                      <input type="text" name="expenses_for_food_clothing_and" class="form-control">
                                    </th>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item -->

                                <table class="table table-bordered">
                                  <tr>
                                    <th>06</th>
                                    <th class="w-50">
                                      Housing expense
                                    </th>
                                    <th>
                                      <input type="text" name="housing_expense" class="form-control">
                                    </th>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item -->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">07</th>
                                    <th>Auto and transportation expense (07A+07B)</th>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <th>SL</th>
                                            <th class="w-50">Content</th>
                                            <th>Amount</th>
                                            <th>Comment</th>
                                          </tr>
                                          <tr>
                                            <td>07-A</td>
                                            <td>
                                              Driver's salary, fuel and maintenance
                                            </td>
                                            <td>
                                              <input type="text" name="drivers_salary_fuel_and_maintenance_amount" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" name="drivers_salary_fuel_and_maintenance_comment" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->
                                          <tr>
                                            <td>07-B</td>
                                            <td>
                                              Other transportation
                                            </td>
                                            <td>
                                              <input type="text" name="other_transportation_amount" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" name="" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">08</th>
                                    <th>Household and utility expenses (08A+08B+08C+08D)</th>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <th>SL</th>
                                            <th class="w-50">Content</th>
                                            <th>Amount</th>
                                            <th>Comment</th>
                                          </tr>
                                          <tr>
                                            <td>08-A</td>
                                            <td>
                                              Electicity
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>08-B</td>
                                            <td>
                                              Gas, water, sewer and garbage
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>08-C</td>
                                            <td>
                                              Phone, internet, TV channels subscription
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>08-D</td>
                                            <td>
                                              Home-support staff and other expenses
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th>09</th>
                                    <th class="w-50">
                                      children's & wife education expenses
                                    </th>
                                    <th>
                                      <input type="text" class="form-control">
                                    </th>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item -->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">10</th>
                                    <th>Special expenses (10A+10B+10C+10D)</th>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <th>SL</th>
                                            <th class="w-50">Content</th>
                                            <th>Amount</th>
                                            <th>Comment</th>
                                          </tr>
                                          <tr>
                                            <td>10-A</td>
                                            <td>
                                              Festival, party, events and gift
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>10-B</td>
                                            <td>
                                              Domestic and overseas tour, holiday, etc
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>10-C</td>
                                            <td>
                                              Donation, philantropy etc
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>10-D</td>
                                            <td>
                                              Other special expenses
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th>11</th>
                                    <th class="w-50">
                                      Any other expenses
                                    </th>
                                    <th>
                                      <input type="text" class="form-control">
                                    </th>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item -->

                                <table class="table table-bordered">
                                  <tr>
                                    <th>12</th>
                                    <th class="w-50">
                                      Total expense relating to lifestyle (05+06+07+08+09+10+11)
                                    </th>
                                    <th>
                                      <input type="text" class="form-control">
                                    </th>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item -->

                                <table class="table table-bordered">
                                  <tr>
                                    <th rowspan="4">13</th>
                                    <th>Payment of tax, charges, etc .(13A+13B)</th>
                                  </tr>
                                  <tr>
                                    <td colspan="3">
                                      <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <th>SL</th>
                                            <th class="w-50">Content</th>
                                            <th>Amount</th>
                                            <th>Comment</th>
                                          </tr>
                                          <tr>
                                            <td>13-A</td>
                                            <td>
                                              Payment of tax at source
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                          <tr>
                                            <td>10-B</td>
                                            <td>
                                              Payment of tax, surcharge or other amount
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>
                                            <td>
                                              <input type="text" class="form-control">
                                            </td>

                                          </tr>
                                          <!-- end inner  single item -->

                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item-->

                                <table class="table table-bordered">
                                  <tr>
                                    <th>14</th>
                                    <th class="w-50">
                                      Total amount of expense and tax (12+13)
                                    </th>
                                    <th>
                                      <input type="text" class="form-control">
                                    </th>
                                  </tr>
                                </table>
                                <!-- end inner table  with single item -->

                                <!-- start name name & signature field -->
                                <div class="row">
                                  <div class="col-lg-12 mb-2">
                                    <p class="mb-1 ">
                                      <label>
                                        15. Verification and signature
                                      </label>
                                    </p>

                                    <p class="mb-0">
                                      I solemnly declare that to the best of my knowledge and belief the informatin given in this statement
                                      and the schedule annexed herwith are correct and complete
                                    </p>
                                  </div>

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="mb-2 d-block">
                                        Name
                                      </label>
                                      <input type="text" class="form-control" placeholder="Enter Name">
                                    </div>
                                  </div><!-- end col -->

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="mb-2 d-block">
                                        Signature & Date
                                      </label>
                                      <input type="file" class="form-control" placeholder="Enter Name">
                                    </div>
                                  </div><!-- end col -->
                                </div>
                                <!-- end name name & signature field -->

                              </td>
                            </tr>
                          </table><!-- end table -->
                        </div><!-- end col -->
                      </div><!-- end row -->
                    </div><!-- end from-card -->

                    <!-- start step button -->
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                    <!-- end step button -->

                  </fieldset>
                  <!-- end Statement of expenses, relating to lifestyle -->

                  <!--  Statement of expenses, relating to lifestyle -->
                  <fieldset>
                    <div class="form-card">
                      <div class="mb-4  text-center">
                        <p class="mb-0"><strong>Mohammad Ali Zinnah Khan</strong></p>
                        <p class="mb-0"><strong>336019506766</strong></p>
                        <p class="mb-0"><strong>Assesment Year : 2019-2020</strong></p>
                        <p class="mb-0"><strong>Computation Sheet</strong></p>
                      </div>

                      <div class="row">
                        <div class="col-lg-12">

                          <table class="table table-bordered">
                            <tr>
                              <th class="w-50">
                                Total Taxable Income
                              </th>
                              <th>
                                <input type="text" class="form-control">
                              </th>
                            </tr>
                          </table>
                          <!-- end  table  with single item -->


                          <table class="table table-bordered">
                            <tbody>
                              <tr>
                                <th>SL</th>
                                <th class="w-50">Content</th>
                                <th>Income On</th>
                                <th>Rate</th>
                                <th>Tax</th>
                              </tr>

                              <tr>
                                <td> 01 </td>
                                <td>
                                  On First
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  0%
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>

                              </tr>
                              <!-- end inner  single item -->

                              <tr>
                                <td> 02 </td>
                                <td>
                                  Next 4,00,000
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  10%
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>

                              </tr>
                              <!-- end inner  single item -->
                              <tr>
                                <td> 03 </td>
                                <td>
                                  Next 5,00,000
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  15%
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>

                              </tr>
                              <!-- end inner  single item -->

                              <tr>
                                <td> 04 </td>
                                <td>
                                  Next 6,00,000
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  20%
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>

                              </tr>
                              <!-- end inner  single item -->

                              <tr>
                                <td> 05 </td>
                                <td>

                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>

                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>

                              </tr>
                              <!-- end inner  single item -->

                              <tr>
                                <td> 06 </td>
                                <td>

                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>

                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>

                              </tr>
                              <!-- end inner  single item -->

                            </tbody>
                          </table>
                          <!-- end table -->

                          <table class="table table-bordered">
                            <tbody>

                              <tr>
                                <td class="w-75">
                                  Total Gross tax before investment Tax rebate
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->

                              <tr>
                                <td>
                                  Less: Investment Tax Rebate
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->
                              <tr>
                                <td>
                                  Tax Liabilities after Tax Rebate
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->
                              <tr>
                                <td>
                                  Less: Tax Deduction ata source on salary
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->
                              <tr>
                                <td>
                                  Less: Tax paid through challan
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->
                              <tr>
                                <td>
                                  Less: Tax Liabilities tax u/s-74
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->

                            </tbody>
                          </table>
                          <!-- end table -->

                          <table class="table table-bordered">
                            <tbody>
                              <tr>
                                <th colspan="5">Investment tax rebate calculation</th>
                              </tr>
                              <tr>
                                <th>Particulars</th>
                                <th>Amount</th>
                                <th>Amount</th>
                                <th>Rate</th>
                                <th>Rebate Amount</th>
                              </tr>

                              <tr>
                                <td>
                                  Actual Investment
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->
                              <tr>
                                <td>
                                  25% of Total income
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->
                              <tr>
                                <td>
                                  TK. 1,50,00,000/-
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                                <td>
                                  <input type="text" class="form-control">
                                </td>
                              </tr>
                              <!-- end inner  single item -->



                            </tbody>
                          </table>
                          <!-- end table -->

                        </div><!-- end col -->
                      </div><!-- end row -->
                    </div><!-- end from-card -->

                    <!-- start step button -->
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="submit" name="next" class="next action-button" value="Submit" />
                    <!-- end step button -->

                  </fieldset>
                  <!-- end Statement of expenses, relating to lifestyle -->

                  <!-- start successfull message -->
                  <fieldset>
                    <div class="form-card">
                      <h2 class="form-card-title text-center">Success !</h2> <br><br>
                      <div class="row justify-content-center">
                        <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                      </div> <br><br>
                      <div class="row justify-content-center">
                        <div class="col-7 text-center">
                          <h5>You Have Successfully Signed Up</h5>
                        </div>
                      </div>
                    </div><!-- end form-card -->
                  </fieldset>
                  <!-- end successfull message -->

                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">
      </div>
    </div>
  </section>
  <!-- right col -->
</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
</section>



<script>
  $(document).ready(function() {


    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;

    $(".next").click(function() {

      current_fs = $(this).parent();
      next_fs = $(this).parent().next();

      //Add Class Active
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({
        opacity: 0
      }, {
        step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            'display': 'none',
            'position': 'relative'
          });
          next_fs.css({
            'opacity': opacity
          });
        },
        duration: 600
      });
    });

    $(".previous").click(function() {

      current_fs = $(this).parent();
      previous_fs = $(this).parent().prev();

      //Remove class active
      $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

      //show the previous fieldset
      previous_fs.show();

      //hide the current fieldset with style
      current_fs.animate({
        opacity: 0
      }, {
        step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            'display': 'none',
            'position': 'relative'
          });
          previous_fs.css({
            'opacity': opacity
          });
        },
        duration: 600
      });
    });

    $('.radio-group .radio').click(function() {
      $(this).parent().find('.radio').removeClass('selected');
      $(this).addClass('selected');
    });

    //Submit add client
    $("form").on('submit', function(e) {
      e.preventDefault();
      var formdata = $(this).serialize()
      console.log(formdata)

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
      });

      $.ajax({
        url: "{{route('store.client')}}",
        type: 'POST',
        data: formdata,
        success: function(data) {
          alert(data)
        }
      })
    })

    //34. Total income (aggregate of 24 to 33)

    $('.income-input').on('change', function() {
      var totalValue = 0
      var thisValue = $(this).val();
      totalValue += parseInt(thisValue);
      console.log(totalValue);
      $('.total-income').html(totalValue)
    })

    //46. Total amount paid and adjusted (42+43+44+45)
    $(".adjust_tax").on('change', function(e) {
      var totalValue = 0
      var thisValue = $(this).val();
      totalValue += parseInt(thisValue);
      console.log(totalValue);
      $('.total_adjust_tax').html(totalValue)
      deficit_or_excess_refunddable()
    })

    //47. Deficit or excess (refunddable)(41-46)
    $(".total_amount_payable").on('change', function() {
      deficit_or_excess_refunddable()
    })

    function deficit_or_excess_refunddable() {
      var refunddable = 0;
      var total_amount_payable = parseInt($('.total_amount_payable').val());
      var total_adjust_tax = parseInt($('.total_adjust_tax').text());
      var nan = isNaN(total_adjust_tax);
      if (nan) {
        total_adjust_tax = 0;
      }
      refunddable = total_amount_payable - total_adjust_tax;
      $('.deficit_or_excess_refunddable').html(refunddable)
    }

    //Schedule 24A
    //taxt_exemptedS24A taxableS24A
    var TotalAmountS24A = 0
    $(document).on('change', '.AmountS24A', function() {
      var taxableS24A = 0
      var AmountS24A = $(this).val()
      var taxt_exemptedS24A = $(this).parents('tr').find('.taxt_exemptedS24A').val()
      taxableS24A = AmountS24A - taxt_exemptedS24A
      $(this).parents('tr').find('.taxableS24A').html(taxableS24A)
      TotalAmountS24A += AmountS24A
      console.log(TotalAmountS24A)
    })

    $(document).on('change', '.taxt_exemptedS24A', function() {
      var taxableS24A = 0
      var AmountS24A = $(this).parents('tr').find('.AmountS24A').val()
      var taxt_exemptedS24A = $(this).val()
      taxableS24A = AmountS24A - taxt_exemptedS24A
      console.log(taxableS24A)
      $(this).parents('tr').find('.taxableS24A').html(taxableS24A)
    })


  });
</script>

<!-- <script>
    $(document).ready(function() {
      

$.validator.setDefaults({
    submitHandler: function(){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
      });
      var list = $('.tableBody').find('.orderData').length
      if (list > 0) {
        //this data will go into sale product invoice details table
        var assessment_year = $("input[name='assessment_year']").val()
        var return_submitted_yes = $("radio[name='return_submitted_yes']").val()
        var return_submitted_no = $("radio[name='return_submitted_no']").val()
        var name_of_the_assessee = $('input[name="name_of_the_assessee"]').val()
        var male = $('radio[name="male"]').val()
        var female = $('radio[name="female"]').val()
        var others = $('radio[name="others"]').val()
        var new_tin = $('input[name="new_tin"]').val()
        var old_tin = $('input[name="old_tin"]').val()
        var circle = $('input[name="circle"]').val()
        var zone = $('input[name="zone"]').val()
        var resident = $('input[name="resident"]').val()
        var freedom_figher = $('input[name="freedom_figher"]').val()
        var disability = $('input[name="disability"]').val()
        var aged_more = $('input[name="aged_more"]').val()
        var egal_guardian = $('input[name="egal_guardian"]').val()
        var date = $('input[name="date"]').val()
        var start_date = $('input[name="start_date"]').val()
        var end_date = $('input[name="end_date"]').val()
        var if_employed = $('input[name="if_employed"]').val()
        var spouse_Name = $('input[name="spouse_Name"]').val()
        var spouse_Tin = $('input[name="spouse_Tin"]').val()
        var father_name = $('input[name="father_name"]').val()
        var mather_name = $('input[name="mather_name"]').val()
        var present_address = $('input[name="present_address"]').val()
        var permanent_address = $('input[name="permanent_address"]').val()
        var contact_telephone = $('input[name="contact_telephone"]').val()
        var nid = $('input[name="nid"]').val()
        var business_identification_number = $('input[name="business_identification_number"]').val()

        //3rd page
        var If_your_a_parent_yes = $('radio[name="If_your_a_parent_yes"]').val()
        var If_your_a_parent_no = $('radio[name="If_your_a_parent_no"]').val()
        var are_you_required_yes = $('radio[name="are_you_required_yes"]').val()
        var are_you_required_no = $('radio[name="are_you_required_no"]').val()
        var schedules_annexed_24a = $('radio[name="schedules_annexed_24a"]').val()
        var schedules_annexed_24b = $('radio[name="schedules_annexed_24b"]').val()
        var schedules_annexed_24c = $('radio[name="schedules_annexed_24c"]').val()
        var schedules_annexed_24d = $('radio[name="schedules_annexed_24d"]').val()
        var statement_annexed_it_10B2016 = $('radio[name="statement_annexed_it_10B2016"]').val()
        var statement_annexed_it_10BB2016 = $('radio[name="statement_annexed_it_10BB2016"]').val()
        var other_statement = $('input[name="other_statement"]').val()
        var challan_no = $('input[name="challan_no"]').val()
        var day = $('input[name="day"]').val()
        var month = $('input[name="month"]').val()
        var year = $('input[name="year"]').val()
        var salary_certificate = $('input[name="salary_certificate"]').val()
        var bank_statement = $('input[name="bank_statement"]').val()
        var name = $('input[name="name"]').val()
        var signature_place = $('input[name="signature_place"]').val()
        var return_submitted_under = $('input[name="return_submitted_under"]').val()
        var tax_Office_entry_number = $('input[name="tax_Office_entry_number"]').val()
        //fourth page
        var total_income_show_34 = $('input[name="total_income_show_34"]').val()
        var amount_payable_serial_34 = $('input[name="amount_payable_serial_34"]').val()
        var amount_payable_serial_41 = $('input[name="amount_payable_serial_41"]').val()
        var amount_paid_46 = $('input[name="amount_paid_46"]').val()
        var amount_of_net_wealth_shown = $('input[name="amount_of_net_wealth_shown"]').val()
        var amount_of_net_wealth_surcharge_paid = $('input[name="amount_of_net_wealth_surcharge_paid"]').val()
        var contact_number_of_tax_Office = $('input[name="contact_number_of_tax_Office"]').val()
        var taxt_office_entry_number = $('input[name="taxt_office_entry_number"]').val()



        var salaies_annex_schedule_24a = $('input[name="salaies_annex_schedule_24a"]').val()
        var interest_on_securities = $('input[name="interest_on_securities"]').val()
        var income_from_house_property = $('input[name="income_from_house_property"]').val()
        var agricultural_income = $('input[name="agricultural_income"]').val()
        var income_from_business_or_profession = $('input[name="income_from_business_or_profession"]').val()
        var capital_gains = $('input[name="capital_gains"]').val()
        var income_from_other_sources = $('input[name="income_from_other_sources"]').val()
        var share_of_income_from_firm_or_aop = $('input[name="share_of_income_from_firm_or_aop"]').val()
        var income_of_minor_or_spouse = $('input[name="income_of_minor_or_spouse"]').val()
        var foreign_income = $('input[name="foreign_income"]').val()
        var total_income = $('input[name="total_income"]').val()
        var gross_tax_before_tax_rebate = $('input[name="gross_tax_before_tax_rebate"]').val()
        var tax_rebate = $('input[name="tax_rebate"]').val()
        var net_tax_after_tax_rebate = $('input[name="net_tax_after_tax_rebate"]').val()
        var minimum_tax = $('input[name="minimum_tax"]').val()
        var net_wealth_surchange = $('input[name="net_wealth_surchange"]').val()
        var interest_or_any_other = $('input[name="interest_or_any_other"]').val()
        var total_amount_payable = $('input[name="total_amount_payable"]').val()
        var tax_deducted = $('input[name="tax_deducted"]').val()
        var advance_tax_paid = $('input[name="advance_tax_paid"]').val()
        var adjustment_of_tax = $('input[name="adjustment_of_tax"]').val()
        var amount_paid = $('input[name="amount_paid"]').val()
        var total_amount = $('input[name="total_amount"]').val()
        var deficit_or_excess_refunddable = $('input[name="deficit_or_excess_refunddable"]').val()
        var tax_exempted = $('input[name="tax_exempted"]').val()


        var basic_pay = $('input[name="basic_pay"]').val()
        var special_pay = $('input[name="special_pay"]').val()
        var arrear_pay = $('input[name="arrear_pay"]').val()
        var dearness_allowance = $('input[name="dearness_allowance"]').val()
        var house_rent_allowance = $('input[name="house_rent_allowance"]').val()
        var medicale_allowance = $('input[name="medicale_allowance"]').val()
        var conveyance_allowance = $('input[name="conveyance_allowance"]').val()
        var festival_allowance = $('input[name="festival_allowance"]').val()
        var allowance_for_support_staff = $('input[name="allowance_for_support_staff"]').val()
        var leave_allowance = $('input[name="leave_allowance"]').val()
        var honorarium_reward_fee = $('input[name="honorarium_reward_fee"]').val()
        var bonus_ex_gratia = $('input[name="bonus_ex_gratia"]').val()
        var house_rent_allowance = $('input[name="house_rent_allowance"]').val()
        var other_allowances = $('input[name="other_allowances"]').val()
        var employers_contribution = $('input[name="employers_contribution"]').val()
        var interest_accrued = $('input[name="interest_accrued"]').val()
        var deemed_income = $('input[name="deemed_income"]').val()
        var deemed_income_for = $('input[name="deemed_income_for"]').val()
        var other_if_any = $('input[name="other_if_any"]').val()
        var total = $('input[name="total"]').val()
        var amount = $('input[name="amount"]').val()
        var taxt_exempted = $('input[name="taxt_exempted"]').val()
        var taxable = $('input[name="taxable"]').val()


        var address_of_the_property = $('input[name="address_of_the_property"]').val()
        var total_area = $('input[name="total_area"]').val()
        var share_of_the_asessee = $('input[name="share_of_the_asessee"]').val()
        var annual_value = $('input[name="annual_value"]').val()
        var repair_collection_etc = $('input[name="repair_collection_etc"]').val()
        var municipal_or_loacal_tax = $('input[name="municipal_or_loacal_tax"]').val()
        var land_revenue = $('input[name="land_revenue"]').val()
        var interest_on_loan = $('input[name="interest_on_loan"]').val()
        var income_from_house_property = $('input[name="income_from_house_property"]').val()
        var income_of_from_house_property_1 = $('input[name="income_of_from_house_property_1"]').val()
        var income_of_from_house_property_2 = $('input[name="income_of_from_house_property_2"]').val()
        var income_of_from_house_property_3 = $('input[name="income_of_from_house_property_3"]').val()

   ///7th page
        var trade_licence = $('input[name="trade_licence"]').val()
        var address = $('input[name="address"]').val()
        var sales_turnover_receipts = $('input[name="sales_turnover_receipts"]').val()
        var gross_profit = $('input[name="gross_profit"]').val()
        var general_administrative = $('input[name="general_administrative"]').val()
        var net_profit = $('input[name="net_profit"]').val()
        var cash_in_hand_at_bank = $('input[name="cash_in_hand_at_bank"]').val()
        var inventories = $('input[name="inventories"]').val()
        var fixed_assets = $('input[name="fixed_assets"]').val()
        var total_asset = $('input[name="total_asset"]').val()
        var opening_capital = $('input[name="opening_capital"]').val()
        var summary_net_profit = $('input[name="summary_net_profit"]').val()
        var withdrawals_in_the_income_year = $('input[name="withdrawals_in_the_income_year"]').val()
        var closing_capital = $('input[name="closing_capital"]').val()
        var liabilities = $('input[name="liabilities"]').val()
        var total_capital_and_liabilities = $('input[name="total_capital_and_liabilities"]').val()

         ///8th page
        var life_insurance_premium = $('input[name="life_insurance_premium"]').val()
        var contribution_to_deposit = $('input[name="contribution_to_deposit"]').val()
        var investment_in_approvedsaving_certificate = $('input[name="investment_in_approvedsaving_certificate"]').val()
        var contribution_to_provident = $('input[name="contribution_to_provident"]').val()
        var self_contribution = $('input[name="self_contribution"]').val()
        var contribution = $('input[name="contribution"]').val()
        var contribution_to_benevolent = $('input[name="contribution_to_benevolent"]').val()
        var contribution_to_zakat_fund = $('input[name="contribution_to_zakat_fund"]').val()
        var total_allowable_investment = $('input[name="total_allowable_investment"]').val()
        var eligible_amount_for_rebate = $('input[name="eligible_amount_for_rebate"]').val()
        var total_allowable_investments = $('input[name="total_allowable_investments"]').val()
        var total_income = $('input[name="total_income"]').val()
        var crore = $('input[name="crore"]').val()
        var amount_of_tax_rebate = $('input[name="amount_of_tax_rebate"]').val()


        var business_capital_other_than_05_b = $('input[name="business_capital_other_than_05_b"]').val()
        var directors_shareholdings_in_limited_companies = $('input[name="directors_shareholdings_in_limited_companies"]').val()
        var non_agricultural_property = $('input[name="non_agricultural_property"]').val()
        var advance_made_for_non_agricultural_property = $('input[name="advance_made_for_non_agricultural_property"]').val()
        var agricultural_property = $('input[name="agricultural_property"]').val()
        var share_debentures_etc = $('input[name="share_debentures_etc"]').val()
        var life_insurance_premium = $('input[name="life_insurance_premium"]').val()
        var fixed_deposit_term_deposits_and_dps = $('input[name="fixed_deposit_term_deposits_and_dps"]').val()
        var share_debentures_etc = $('input[name="share_debentures_etc"]').val()
        var loans_given_to_others_mention_name_and_tin = $('input[name="loans_given_to_others_mention_name_and_tin"]').val()
        var other_financial_assets_give_deatils = $('input[name="other_financial_assets_give_deatils"]').val()
        var motro_cars = $('input[name="motro_cars"]').val()
        var brand_name = $('input[name="brand_name"]').val()
        var engine_cc = $('input[name="engine_cc"]').val()
        var registration_no = $('input[name="registration_no"]').val()
        var mention_quantity = $('input[name="mention_quantity"]').val()
        var furniture_equipment_and_electronics_items = $('input[name="furniture_equipment_and_electronics_items"]').val()
        var other_assets_of_significant_value_jewellery = $('input[name="other_assets_of_significant_value_jewellery"]').val()
        var cash_and_fund_outside_business_13A_13B_13C_13D = $('input[name="cash_and_fund_outside_business_13A_13B_13C_13D"]').val()
        var notes_and_currencies = $('input[name="notes_and_currencies"]').val()
        var banks_cards_and_electronic_cash = $('input[name="banks_cards_and_electronic_cash"]').val()
        var provident_fund_and_ohter_fund = $('input[name="provident_fund_and_ohter_fund"]').val()
        var other_deposits_balance_and_advance_other_than_8 = $('input[name="other_deposits_balance_and_advance_other_than_8"]').val()
        var gross_wealth_aggregate_of_05_to_13 = $('input[name="gross_wealth_aggregate_of_05_to_13"]').val()
        var liabilities_outside_business_15A_15B_15C = $('input[name="liabilities_outside_business_15A_15B_15C"]').val()
        var borrowdings_from_banks_other_finincial_instirutions = $('input[name="borrowdings_from_banks_other_finincial_instirutions"]').val()

        var unsecured_loadn_mention_name_and_tin = $('input[name="unsecured_loadn_mention_name_and_tin"]').val()
        var other_loans_or_overdrafts = $('input[name="other_loans_or_overdrafts"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth_at_the_last_date_of_the_pervious_income_year = $('input[name="net_wealth_at_the_last_date_of_the_pervious_income_year"]').val()
        var change_in_net_wealth = $('input[name="change_in_net_wealth"]').val()
        var other_fund_outflow_during_the_income_year_19A_19B_19C = $('input[name="other_fund_outflow_during_the_income_year_19A_19B_19C"]').val()
        var annual_living_expenditure_and_tax_payments_as_it_10BB2016 = $('input[name="annual_living_expenditure_and_tax_payments_as_it_10BB2016"]').val()
        var loss_deductions_expenses_etc_not_mentioned_in_IT_10BB2016 = $('input[name="loss_deductions_expenses_etc_not_mentioned_in_IT_10BB2016"]').val()
        var total_fund_outflow_in_the_income_year_18_19 = $('input[name="total_fund_outflow_in_the_income_year_18_19"]').val()
        var sources_of_fund_21A_21B_21C = $('input[name="sources_of_fund_21A_21B_21C"]').val()
        var income_shown_in_the_return = $('input[name="income_shown_in_the_return"]').val()
        var taxexempted_income_and_allowance = $('input[name="taxexempted_income_and_allowance"]').val()
        var other_receipts_gift_from_father_mother = $('input[name="other_receipts_gift_from_father_mother"]').val()
        var shortage_of_fund_if_any_2021 = $('input[name="shortage_of_fund_if_any_2021"]').val()
        var sl = $('input[name="sl"]').val()
        var content = $('input[name="content"]').val()
        var payment_of_tax_surcharge_or_other_amount = $('input[name="payment_of_tax_surcharge_or_other_amount"]').val()

        var shareholdings_in_limited_companies_as_director = $('input[name="shareholdings_in_limited_companies_as_director"]').val()
        var no_of_shares = $('input[name="no_of_shares"]').val()
        var value = $('input[name="value"]').val()
        var description_location_and_size = $('input[name="description_location_and_size"]').val()
        var non_agricultural_property_at_cost_value = $('input[name="non_agricultural_property_at_cost_value"]').val()
        var value_at_the_start_of_income_year = $('input[name="value_at_the_start_of_income_year"]').val()
        var increased_decreased_during_the_income_year = $('input[name="increased_decreased_during_the_income_year"]').val()
        var value_at_the_last_date_of_income_year = $('input[name="value_at_the_last_date_of_income_year"]').val()
        var agricultural_property_at_cost_value = $('input[name="agricultural_property_at_cost_value"]').val()
        var value_at_the_start_of_income_year = $('input[name="value_at_the_start_of_income_year"]').val()
        var increased_decreased_during_the_income_year = $('input[name="increased_decreased_during_the_income_year"]').val()
        var value_at_the_last_date_of_income_year = $('input[name="value_at_the_last_date_of_income_year"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()
        var net_wealth = $('input[name="net_wealth"]').val()

        var items = $('.totalItems').text()
        var total = $('#grandtotal').text()
        var totalOrderTax = $('.totalorderTax').text()
        var grandTotal = $('.grossTotal').text()


        //from order table
        //this data will go into sale product deatails table
        products = []
        $(".tableBody").find('.orderData').each(function() {
          let quantity = $(this).find('.quantity').val()
          let sale_unit_id = $(this).find('.select-sale-unit option:selected').attr('data-unit-id')
          let sale_unit_value = $(this).find('.select-sale-unit option:selected').val() //For Stock management
          let sale_price = $(this).find('.net-unit-cost').val()
          let subtotal = $(this).find('.subtotal').text()
          let product_id = $(this).attr('data-id')
          product_data = {
            "quantity": quantity,
            "subtotal": subtotal,
            "product_id": product_id,
            "sale_unit_id": sale_unit_id,
            "sale_unit_value": sale_unit_value,
            "sale_price": sale_price,

          }
          products.push(product_data)


        });

        console.log(products)

        var form = $('AddPurchase')[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        formData.append('reference_no', reference_no);
        formData.append('input_customer', input_customer);
        formData.append('select_customer', select_customer);
        formData.append('warehouse', warehouse);
        formData.append('biller', biller);
        formData.append('orderTax', orderTax);
        formData.append('orderDiscount', orderDiscount);
        formData.append('shippingCost', shippingCost);
        formData.append('document', document);
        formData.append('sale_status', sale_status);
        formData.append('payment_status', payment_status);
        formData.append('paid_by_id', paid_by_id);
        formData.append('receive_amount', receive_amount);
        formData.append('paid_amount', paid_amount);
        formData.append('cheque_no', cheque_no);
        formData.append('bank', bank);
        formData.append('bank_branch', bank_branch);
        formData.append('sale_note', sale_note);
        formData.append('stuff_note', stuff_note);
        formData.append('items', items);
        formData.append('total', total);
        formData.append('totalOrderTax', totalOrderTax);
        formData.append('grandTotal', grandTotal);
        formData.append('products', JSON.stringify(products));

        $.ajax({
          url: "{{route('store.client')}}",
          type: 'POST',
          data: formData,
          contentType: false,
          processData: false,
          success: function(resp) {
            console.log(resp)
            if (resp.success) {

              Toast.fire({
                icon: 'success',
                title: resp.message
              })

              window.location.replace("{{route('client.view')}}");
            } else {
              Toast.fire({
                icon: 'danger',
                title: resp.message
              })
            }

          }
        })
      } else {
        Toast.fire({
          icon: 'warning',
          title: "Please select product"
        })
      }
    }
  });
});
</script> -->


@endsection