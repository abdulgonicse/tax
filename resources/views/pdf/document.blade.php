<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <title>Tax Return </title>

   <style>
      * {
         margin: 0;
         padding: 0;
         box-sizing: border-box;
      }

      ul {
         margin: 0;
         padding: 0;
         list-style: none;
      }

      .text-left {
         text-align: left;
      }

      .text-center {
         text-align: center;
      }

      .text-right {
         text-align: right;
      }

      /*form control*/
      .box-sm-box {
         min-width: 40px;
         min-height: 40px;
         border: .5px solid #222;
         display: flex;
         align-items: center;
         justify-content: center;
         text-align: center;
         line-height: 40px;
         padding: 20px;
      }

      .form-group .form-control:focus::placeholder {
         opacity: 0;
      }

      .form-group {
         margin-bottom: 0;
      }

      input:focus {
         outline: 0;
         box-shadow: none;
      }

      .form-control:focus {
         outline: 0;
         box-shadow: none;
      }

      /*form-card-additional-list*/

      .form-card-additional-list li {
         margin-bottom: 3px;
         list-style: none;
      }

      .form-card-additional-list li span {
         font-weight: 600;
      }

      /*profile pircute image*/
      .profile-picture img {
         width: 150px;
         height: 150px;
         object-fit: cover;
      }

      /*table*/
      .table {
         margin-bottom: 0;
      }

      .label,
      .sl_no {
         font-weight: 600;

      }

      .label {
         margin-bottom: 5px;
      }

      .form-title {
         margin-bottom: 5px;
      }

      .table td,
      .table th {
         padding: 0.6rem;
      }

      .table-bordered td,
      .table-bordered th {
         border: .5px solid #222;
      }

      /*submitted_data*/
      .submitted_data {
         color: #616161;
      }
   </style>
</head>

<body style="margin: 0; padding: 0; font-size: 16px; line-height: 22px;">

   <!-- start  page layout 1-->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Return Of Income
            </span> <br />
            For and Individual Assessee
         </h4>
      </div>

      <table>
         <tr>
            <td style="width: 75%;">
               <!-- start additional text  -->
               <div style="margin-bottom: 10px;">
                  <div class="form-card-additional-text" style="padding-right: 30px;">
                     <div style="margin-bottom: 15px;">
                        <strong>
                           The following schedules shall be the integral part of this reutrn and must be annexed
                           to reutrn in the following cases :
                        </strong>
                     </div>
                     <div class="form-card-additional-list">
                        <div style="list-style: none;">
                           <strong>Schedule 24-A :</strong> if you have income from salaries .
                        </div>
                        <div>
                           <strong>Schedule 24-B :</strong> if you have income from house Property .
                        </div>
                        <div>
                           <strong>Schedule 24-C :</strong> if you have income from business or profession .
                        </div>
                        <div>
                           <strong>Schedule 24-D :</strong> if you claim tax rebate .
                        </div>
                     </div>
                  </div>

               </div>
            </td>
            <!-- <td style="width: 25%;">
               <div class="profile-picture" style="text-align:right; margin-left:auto">
                  <img src="./profile.jpeg" width="150" height="150" alt="">
               </div>
            </td> -->
         </tr>
      </table>

      <!-- end additional text  -->

      <!-- start page header -->
      <div style="text-align: center; margin: 10px 0;">
         <div style="margin-bottom: 0;"><strong>Part 1</strong></div>
         <div>Basic Information</div>
      </div>
      <!-- end page header -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 100px">
            <col style="width: 50px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 305px">
            <col style="width: 200px">
            <col style="width: 50px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 305px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no" rowspan="2">01</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label">
                        Assessment Year
                     </label>
                  </div>
               </td>
               <td class="sl_no" rowspan="2">02</td>
               <td colspan="5">
                  <label class="label">
                     Return submitted under section 82BB(Select One)
                  </label>
               </td>
            </tr>
            <tr>
               <td colspan="5">
                  <div class="multiple-input-box-area">
                     <span class="box-sm-box submitted_data" style="max-width: 40px; max-height:40px">2</span>
                     <span class="box-sm-box submitted_data">0</span>
                     <span class="box-sm-box submitted_data">1</span>
                     <span class="box-sm-box submitted_data">9</span>
                     <span class="box-sm-box" style="border: none; width: 20px;">--</span>

                     <span class="box-sm-box submitted_data">2</span>
                     <span class="box-sm-box submitted_data">0</span>
                  </div>
               </td>
               <td colspan="5">
                  <div class="section-82BB-data submitted_data pt-2">Yes</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name of the Assessee
                     </label>
                     <div class="assessee-name font-italic submitted_data">Mohammad Ali Zinnah Khan</div>
                  </div>
               </td>
               <td class="sl_no">04</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Gender
                     </label>
                     <div class="assessee-gender submitted_data">Male</div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">05</td>
               <td colspan="5">
                  <div class="form-group d-flex">
                     <label class="label mb-0 mr-4">
                        12-digit Tin:
                     </label>
                     <div class="tin-number submitted_data">336019506766</div>
                  </div>
               </td>
               <td class="sl_no">06</td>
               <td colspan="5">
                  <div class="form-group d-flex">
                     <label class="label mb-0 mr-4">
                        Old Tin:
                     </label>
                     <div class="tin-number-old submitted_data">XXXXXXXXXXXX</div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">07</td>
               <td colspan="5">
                  <div class="form-group d-flex">
                     <label class="label mb-0 mr-4">
                        Circle:
                     </label>
                     <div class="circle-salary submitted_data">149 ( Salary)</div>
                  </div>
               </td>
               <td class="sl_no">08</td>
               <td colspan="5">
                  <div class="form-group d-flex">
                     <label class="label mb-0 mr-4">
                        Zone:
                     </label>
                     <div class="select-zone submitted_data">Dhaka</div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">09</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-0 d-block">
                        Resident Status (Tick One)
                     </label>
                  </div>
               </td>

               <td colspan="6">
                  <div class="resident-status submitted_data">
                     Resident
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no" rowspan="3">10</td>
               <td colspan="11">
                  <label class="label mb-0 d-block">
                     Tick on the box(es) below if you are
                  </label>
               </td>
            </tr>
            <tr>
               <td class=" sl_no">10A</td>
               <td colspan="4">
                  <div class="form-check-label_1 submitted_data">
                     A Gazetted war-wounded freedom figher
                  </div>
               </td>
               <td class="sl_no">10B</td>
               <td colspan="5">
                  <div class="form-check-label_2 submitted_data">
                     A person with disability
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">10C</td>
               <td colspan="4">
                  <div class="form-check-label_3 submitted_data">
                     Aged 65 years or more
                  </div>
               </td>
               <td class="sl_no">10D</td>
               <td colspan="5">
                  <div class="form-check-label_4 submitted_data">
                     A parent/legal guardian of a
                     person with disability
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no" rowspan="2">11</td>
               <td colspan="5">
                  <label class=" label mb-0 d-block">
                     Date of Birth (DD-MM-YYYY)
                  </label>
               </td>
               <td class="sl_no" rowspan="2">12</td>
               <td colspan="5">
                  <label class=" label mb-0 d-block">
                     Income Year
                  </label>
               </td>
            </tr>
            <tr>
               <td colspan="5">
                  <div class="multiple-input-box-area">
                     <span class="box-sm-box submitted_data">1</span>
                     <span class="box-sm-box submitted_data">5</span>
                     <span class="box-sm-box submitted_data">0</span>
                     <span class="box-sm-box submitted_data">4</span>
                     <span class="box-sm-box submitted_data">1</span>
                     <span class="box-sm-box submitted_data">9</span>
                     <span class="box-sm-box submitted_data">7</span>
                     <span class="box-sm-box submitted_data">2</span>
                  </div>
               </td>
               <td colspan="5">
                  <div class="form-group">
                     <div class="income-year-box">
                        <div class="income-year-start submitted_data">01/07/2018</div>
                        <span>to</span>
                        <div class="income-year-end submitted_data">30/06/2019</div>
                     </div>
                  </div>
               </td>
            </tr>

            <tr>
               <td class="sl_no">13</td>
               <td colspan="11">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        If Employed
                     </label>
                     <div class="employed-deatils submitted_data">
                        Employer's Name: center for HIV and AIDS, ICDDRB, 69 shaheed tajuddin ahmed sarani,
                        Mohakhali,
                        Dhaka - 1212
                     </div>
                  </div>
               </td>
            </tr>

            <tr>
               <td class="sl_no">14</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Spouse Name:
                     </label>
                     <div class="spouse-name submitted_data">
                        Monira Mozid
                     </div>
                  </div>
               </td>
               <td class="sl_no">15</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Spouse TIN (if any)
                     </label>
                     <div class="spouse-tin submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">16</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Father's Name:
                     </label>
                     <div class="father-name submitted_data">
                        Late Abdus Samad Khan
                     </div>
                  </div>
               </td>
               <td class="sl_no">17</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Mother's Name:
                     </label>
                     <div class="mother-name submitted_data">
                        Mrs. Morzina Khatun
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">18</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Present Address:
                     </label>
                     <div class="present-address submitted_data">
                        House - 04, Flat-D-2(4th Floor), Rupnagar Residential Area, Mirpur-1216, Dhaka.
                     </div>
                  </div>
               </td>
               <td class="sl_no">19</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Permanent Address:
                     </label>
                     <div class="permanent-address submitted_data">
                        Vill: XYZ, P.O: Kalidas Ganti, Upazilla: Sirgajgonj, Dist: Sirgajgonj.
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">20</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Contact Telephone:
                     </label>
                     <div class="contact-no submitted_data">
                        01712280244
                     </div>
                  </div>
               </td>
               <td class="sl_no">21</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        E-mail :
                     </label>
                     <div class="email-address submitted_data">
                        zkhan@gmail.com
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">22</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        National Identification Number:
                     </label>
                     <div class="nid-no submitted_data">
                        7757510891
                     </div>
                  </div>
               </td>
               <td class="sl_no">23</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Business Identification Number(s):
                     </label>
                     <div class="business-id-no submitted_data">

                     </div>
                  </div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->
   </div>
   <!-- end  page layout 1-->

   <!-- start  page layout 2-->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Part 2
            </span> <br />
            Particulars of Income and Tax
         </h4>
      </div>

      <div class="tin-number-boxed" style="text-align: center; margin-bottom:20px">
         <div>TIN : </div>
         <div class="multiple-input-box-area">
            <span class="box-sm-box submitted_data">3</span>
            <span class="box-sm-box submitted_data">3</span>
            <span class="box-sm-box submitted_data">6</span>
            <span class="box-sm-box submitted_data">0</span>
            <span class="box-sm-box submitted_data">1</span>
            <span class="box-sm-box submitted_data">9</span>
            <span class="box-sm-box submitted_data">5</span>
            <span class="box-sm-box submitted_data">0</span>
            <span class="box-sm-box submitted_data">6</span>
            <span class="box-sm-box submitted_data">7</span>
            <span class="box-sm-box submitted_data">6</span>
            <span class="box-sm-box submitted_data">6</s>
         </div>
      </div>
      <!-- end page header -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 80px">
            <col style="width: 500px">
            <col style="width: 120px">
            <col style="width: 300px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label">SL NO</td>
               <td class="label">Statement of total income</td>
               <td class="label">Section</td>
               <td class="label">Amount</td>
            </tr>
            <tr>
               <td class="sl_no">24</td>
               <td>Salaies (annex Schedule 24A)</td>
               <td>S.21</td>
               <td class="text-right">
                  <div class="submitted_data">
                     791,423
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">25</td>
               <td>Interest on securities</td>
               <td>S.22</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">26</td>
               <td>Income from house property(annex Schedule 24B)</td>
               <td>S.24</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">27</td>
               <td>Agricultural income</td>
               <td>S.26</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">28</td>
               <td>
                  Income from business or profession (annex Schedule 24c)
               </td>
               <td>S.28</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">29</td>
               <td>Capital gains</td>
               <td>S.31</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">30</td>
               <td>Income from other sources</td>
               <td>S.33</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">31</td>
               <td>Share of income from firm or AOP</td>
               <td></td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">32</td>
               <td>Income of minor or spouse under section 43(4)</td>
               <td>S.43</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">33</td>
               <td>Foreign income</td>
               <td></td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">34</td>
               <td>Total income (aggregate of 24 to 33)</td>
               <td></td>
               <td class="text-right">
                  <div class="submitted_data">
                     791,423
                  </div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 80px">
            <col style="width: 620px">
            <col style="width: 300px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label">SL NO</td>
               <td class="label">Tax Computation and Payment</td>
               <td class="label">Amount</td>
            </tr>
            <tr>
               <td class="sl_no">35</td>
               <td>Gross tax before tax rebate</td>
               <td class="text-right">
                  <div class="submitted_data">
                     61,213
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">36</td>
               <td>Tax rebate (annex Schedule 24D)</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">37</td>
               <td>Net tax after tax rebate</td>
               <td class="text-right">
                  <div class="submitted_data">
                     61,213
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">38</td>
               <td>Minimum tax</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">39</td>
               <td>Net wealth surchange</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">40</td>
               <td>
                  Interest or any other amount under the Ordinance (if any)
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">41</td>
               <td>Total amount payable</td>
               <td class="text-right">
                  <div class="submitted_data">
                     61,213
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">42</td>
               <td>
                  Tax deducted or collected at source (attach proof)
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     25,900
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">43</td>
               <td>
                  Advance tax paid (attach proof)
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">44</td>
               <td>
                  Adjustment of tax refund [mention assessment
                  year(s) of refund]
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">45</td>
               <td>
                  Amount paid with return (attach proof) U/S 74
                  (Challan No. Dated: 11/2019, Mohakhali Branch,
                  Sonali Bank Limited)
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     35,313
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">46</td>
               <td>
                  Total amount paid and adjusted (42 + 43 + 44 + 45)
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     61,213
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">47</td>
               <td>
                  Deficit or excess (refunddable)(41-46)
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     0
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">48</td>
               <td>
                  Tax exempted income
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     377,736
                  </div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end  page layout 2-->

   <!-- start  page layout 3-->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start page header -->
      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Part 3
            </span> <br />
            Instruction, Enclosure and Verification
         </h4>
      </div>
      <div class="tin-number-boxed" style="text-align: center; margin-bottom:20px">
         <div>TIN : </div>
         <div class="multiple-input-box-area">
            <span class="box-sm-box submitted_data">3</span>
            <span class="box-sm-box submitted_data">3</span>
            <span class="box-sm-box submitted_data">6</span>
            <span class="box-sm-box submitted_data">0</span>
            <span class="box-sm-box submitted_data">1</span>
            <span class="box-sm-box submitted_data">9</span>
            <span class="box-sm-box submitted_data">5</span>
            <span class="box-sm-box submitted_data">0</span>
            <span class="box-sm-box submitted_data">6</span>
            <span class="box-sm-box submitted_data">7</span>
            <span class="box-sm-box submitted_data">6</span>
            <span class="box-sm-box submitted_data">6</s>
         </div>
      </div>
      <!-- end page header -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 950px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no">49</td>
               <td>
                  <div class="mb-1 label"><strong>Instructions :</strong></div>
                  <div class="form-card-additional-list list-2">
                     <div>
                        <strong>1. </strong>
                        Statement of assets, liabilities and expenses
                        (IT-10B2016) and statement of life style expense (IT-
                        10BB2016) must be furnished with the return unless you
                        are exempted from furnishing such statement(s) under
                        section 80.
                     </div>
                     <div>
                        <strong>2. </strong>
                        Proof of payments of tax, including Advance tax
                        and withholding tax and the proof of investment for tax
                        rebate must be provided along with return.
                     </div>
                     <div>
                        <strong>3. </strong>
                        Attach account statements and other documents where
                        applicable
                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 190px">
            <col style="width: 190px">
            <col style="width: 190px">
            <col style="width: 190px">
            <col style="width: 190px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no">50</td>
               <td colspan="4">
                  If your a parent of a person with disability, has
                  your spouse availed the extended tax exemption
                  threshold?(tick one)
               </td>
               <td>
                  <div class="submitted_data">Yes</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">51</td>
               <td colspan="4">
                  Are you required to submit a statement of assets,
                  liabilities and expenses (IT-10b2016) under section
                  80(1) ? (tick one)
               </td>
               <td>
                  <div class="submitted_data">Yes</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">52</td>
               <td colspan="3">
                  Schedules annexed (tick all that are applicable)
               </td>
               <td colspan="2">
                  <div class="submitted_data">

                     <span class="list-inline-item">24 A</span>
                     <span class="list-inline-item">24 B</span>
                     <span class="list-inline-item">24 C</span>
                     <span class="list-inline-item">24 D</span>

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">53</td>
               <td colspan="3">
                  Schedules annexed (tick all that are applicable)
               </td>
               <td colspan="2">
                  <div class="submitted_data">

                     <span class="list-inline-item">IT-10B2016</span>
                     <span class="list-inline-item">IT-10B2016</span>

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">54</td>
               <td colspan="5">
                  <div class="mb-1 label">Other Statement, documents, etc. attached (list all)</div>
                  <div>
                     <strong>1. </strong>
                     <span class="mr-4">Challan No : </span> <span class="challan_no submitted_data"></span>
                     <span class="mr-4">Dated : </span> <span class="date submitted_data"></span>
                  </div>
                  <div>
                     <strong>2. </strong>
                     Income Tax Computation Sheet <span class="submitted_data"></span>
                  </div>
                  <div>
                     <strong>3. </strong>
                     Salary Certificate <span class="submitted_data"></span>
                  </div>
                  <div>
                     <strong>4. </strong>
                     Bank statement <span class="submitted_data"></span>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <div style="margin-bottom: 0;"><strong>Verification and signature</strong></div>
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 475px">
            <col style="width: 475px">
         </colgroup>
         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no" rowspan="3">55</td>
               <td colspan="2">
                  <strong>Verification</strong> <br>
                  I solemnly declare that to the best of my
                  knowledge and belief the information given in this retrn and
                  statements and documents annexed or attached here with are
                  correct and complete.
               </td>
            </tr>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name :
                     </label>
                     <div class="submitted_data">Mohammad Ali Zinnah Khan</div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Signature :
                     </label>
                     <div class="submitted_data"></div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Date of Signature (DD-MM-YYYY) :
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">5</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">4</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">9</span>
                        <span class="box-sm-box submitted_data">7</span>
                        <span class="box-sm-box submitted_data">2</span>
                     </div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Place of Signature :
                     </label>
                     <div class="submitted_data"></div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->


      <div style="text-align: center;">
         <div class="mb-0" style="margin-bottom: 0;"><strong>For the official use only</strong></div>
         <div class="mb-1" style="margin-bottom: 0;">Return Submission Information</div>
      </div>
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 500px">
            <col style="width: 500px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Date of Submission (DD-MM-YYYY) :
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">5</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">4</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">9</span>
                        <span class="box-sm-box submitted_data">7</span>
                        <span class="box-sm-box submitted_data">2</span>
                     </div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Tax Office Entry Number :
                     </label>
                     <div class="submitted_data"></div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end  page layout 3-->

   <!-- start  page layout 4-->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Acknowledgement Receipt of
            </span> <br />
            Return of Income
         </h4>
      </div>

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 320px">
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 320px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no" rowspan="2">01</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label">
                        Assessment Year
                     </label>
                  </div>
               </td>
               <td class="sl_no" rowspan="2">02</td>
               <td colspan="5">
                  <label class="label">
                     Return submitted under section 82BB(Select One)
                  </label>
               </td>
            </tr>
            <tr>
               <td colspan="5">
                  <div class="multiple-input-box-area">
                     <span class="box-sm-box submitted_data">2</span>
                     <span class="box-sm-box submitted_data">0</span>
                     <span class="box-sm-box submitted_data">1</span>
                     <span class="box-sm-box submitted_data">9</span>
                     <span class="box-sm-box submitted_data">2</span>
                     <span class="box-sm-box submitted_data">0</span>
                  </div>
               </td>
               <td colspan="5">
                  <div class="section-82BB-data submitted_data pt-2">Yes</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td colspan="11">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name of the Assessee
                     </label>
                     <div class="assessee-name font-italic submitted_data">Mohammad Ali Zinnah Khan</div>
                  </div>
               </td>

            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td colspan="5">
                  <div class="form-group d-flex">
                     <label class="label mb-0 mr-4">
                        12-digit Tin:
                     </label>
                     <div class="tin-number submitted_data">336019506766</div>
                  </div>
               </td>
               <td class="sl_no">05</td>
               <td colspan="5">
                  <div class="form-group d-flex">
                     <label class="label mb-0 mr-4">
                        Old Tin:
                     </label>
                     <div class="tin-number-old submitted_data">XXXXXXXXXXXX</div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">06</td>
               <td colspan="5">
                  <div class="form-group d-flex">
                     <label class="label mb-0 mr-4">
                        Circle:
                     </label>
                     <div class="circle-salary submitted_data">149 ( Salary)</div>
                  </div>
               </td>
               <td class="sl_no">07</td>
               <td colspan="5">
                  <div class="form-group d-flex">
                     <label class="label mb-0 mr-4">
                        Zone :
                     </label>
                     <div class="select-zone submitted_data">Dhaka</div>
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="12">
                  <div class="form-group">
                     <label class="label mb-0 d-block">
                        Total income shown (Serial 34)
                     </label>
                     <div class="submitted_data">
                        791,423
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="6">
                  <div class="form-group">
                     <label class="label mb-0 d-block">
                        Amount Payable (serial 41)
                     </label>
                     <div class="submitted_data">
                        61,213
                     </div>
                  </div>
               </td>
               <td colspan="6">
                  <div class="form-group">
                     <label class="label mb-0 d-block">
                        Amount paid and adjusted(serial 46)
                     </label>
                     <div class="submitted_data">
                        61,213
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="6">
                  <div class="form-group">
                     <label class="label mb-0 d-block">
                        Amount of net wealth shown in IT 10BB2016
                     </label>
                     <div class="submitted_data">
                        3,335,428
                     </div>
                  </div>
               </td>
               <td colspan="6">
                  <div class="form-group">
                     <label class="label mb-0 d-block">
                        Amount of net wealth surcharge paid
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="6">
                  <div class="form-group">
                     <label class=" label mb-2 d-block">
                        Date of Birth (DD-MM-YYYY)
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">5</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">4</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">9</span>
                        <span class="box-sm-box submitted_data">7</span>
                        <span class="box-sm-box submitted_data">2</span>
                     </div>
                  </div>
               </td>
               <td colspan="6">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Tax Office Entry Number
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="12">
                  <div class="form-group">
                     <label class="label mb-0 d-block">
                        Signature and seal of the official receiving the reutrn
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="6">
                  <div class="form-group">
                     <label class=" label mb-2 d-block">
                        Date of Signature (DD-MM-YYYY)
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">5</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">4</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">9</span>
                        <span class="box-sm-box submitted_data">7</span>
                        <span class="box-sm-box submitted_data">2</span>
                     </div>
                  </div>
               </td>
               <td colspan="6">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Contact Number of Tax Office
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->
   </div>
   <!-- end  page layout 4-->

   <!-- start page layout 5-->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Schedule 24A
            </span>
         </h4>
         <div>
            Particulars of income from salaries <br />
            Annex this schedule to the return of income if you have income from salaries
         </div>
      </div>

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 450px">
            <col style="width: 50px">
            <col style="width: 450px">

         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no">01</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        Assessment Year
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                     </div>
                  </div>
               </td>
               <td class="sl_no">02</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        TIN :
                     </label>
                     <div class="submitted_data">
                        336019506766
                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 265px">
            <col style="width: 265px">
            <col style="width: 140px">
            <col style="width: 140px">
            <col style="width: 140px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label" colspan="3">Particulars</td>
               <td class="label">Amount (A)</td>
               <td class="label">Tax Exempted (B)</td>
               <td class="label">Taxable (C = A-B)</td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td colspan="2">
                  Basic Pay
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td colspan="2">
                  Special Pay
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">05</td>
               <td colspan="2">
                  Arrear Pay (if not included in taxable income
                  earlier)
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">06</td>
               <td colspan="2">
                  Dearness allowance
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">07</td>
               <td colspan="2">
                  House rent allowance
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">08</td>
               <td colspan="2">
                  Medical allowance
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">09</td>
               <td colspan="2">
                  Conveyance allowance
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">10</td>
               <td colspan="2">
                  Festival Allowance
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">11</td>
               <td colspan="2">
                  Allowance for support staff
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">12</td>
               <td colspan="2">
                  Leave allowance
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">13</td>
               <td colspan="2">
                  Honorarium/Reward/Fee
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">14</td>
               <td colspan="2">
                  Overtime Allowance
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">15</td>
               <td colspan="2">
                  Bonus / Ex-gratia
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">16</td>
               <td colspan="2">
                  Other Allowances
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">17</td>
               <td colspan="2">
                  Employer's contribution to a recognized provident
                  fund
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">18</td>
               <td colspan="2">
                  Interest accrued on a recognized provident provident
                  fund
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">19</td>
               <td colspan="2">
                  Deemed income income for transort facility
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">20</td>
               <td colspan="2">
                  Deemed income for free furnished/unfurnished
                  accommodation
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">21</td>
               <td colspan="2">
                  Other, if any (give detail)
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>
            <tr>
               <!-- <td class="sl_no">22</td> -->
               <td class="label text-center" colspan="3">
                  Total
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">0</div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->
      <div><strong>All Figures of amount are in taka</strong></div>
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">


         <colgroup>
            <col style="width: 500px">
            <col style="width: 500px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name :
                     </label>
                     <div class="submitted_data">
                        Mohammad Ali Zinnah Khan
                     </div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Signature & Date
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end  page layout 5 -->

   <!-- start page layout 6 -->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start page header -->
      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Schedule 24B
            </span>
         </h4>
         <div>
            Particulars of income from House Property <br />
            Annex this schedule to the return of income if you have income from House Property
         </div>
      </div>
      <!-- end page header -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 450px">
            <col style="width: 50px">
            <col style="width: 450px">

         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no">01</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        Assessment Year
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                     </div>
                  </div>
               </td>
               <td class="sl_no">02</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        TIN :
                     </label>
                     <div class="submitted_data">
                        336019506766
                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <div class="mb-0"><strong>For each House Property</strong></div>
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 320px">
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 25px">
            <col style="width: 320px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no" rowspan="3">03</td>
               <td colspan="11">
                  <label class="label mb-0 d-block">
                     Description of the house property
                  </label>
               </td>
            </tr>
            <tr>
               <td class="sl_no" rowspan="2">03A</td>
               <td colspan="4" rowspan="2">
                  <div class="form-group">
                     <label class="label mb-2 d-block">Address of the property</label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
               <td class="sl_no">03B</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">Total area</label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">03C</td>
               <td colspan="5">
                  <div class="form-group">
                     <label class="label mb-2 d-block">Share of the asessee(%)</label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px;">
            <col style="width: 50px;">
            <col style="width: 190px;">
            <col style="width: 190px;">
            <col style="width: 190px;">
            <col style="width: 190px;">
            <col style="width: 140px;">
         </colgroup>

         <tbody>
            <tr class="text-center">
               <td class="label" colspan="6">
                  Income from House Property
               </td>
               <td class="label">
                  Amount
               </td>
            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td colspan="5">Annual Value</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>

            <!-- start 5 -->
            <tr>
               <td class="sl_no" rowspan="8">05</td>
               <td colspan="5">Deducations (aggregate of 05A to 05G)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  05A
               </td>
               <td colspan="4">Repair, collection etc</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  05B
               </td>
               <td colspan="4">Municipal or loacal Tax</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  05C
               </td>
               <td colspan="4">Land Revenue</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  05D
               </td>
               <td colspan="4">Interest on Loan/Mortage/capital charge</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  05E
               </td>
               <td colspan="4">Insurance Premium</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  05F
               </td>
               <td colspan="4">Vacancy Allowance</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  05G
               </td>
               <td colspan="4">Other, if any</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end 5 -->

            <tr>
               <td class="sl_no">06</td>
               <td colspan="5">Income from house property(04-05)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">07</td>
               <td colspan="5">Incase of partial ownership, the share of income</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <div><strong>Provide information if income from more than one house property</strong></div>
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px;">
            <col style="width: 50px;">
            <col style="width: 190px;">
            <col style="width: 190px;">
            <col style="width: 190px;">
            <col style="width: 190px;">
            <col style="width: 140px;">
         </colgroup>

         <tbody>
            <!-- start 8 -->
            <tr>
               <td class="sl_no" rowspan="4">08</td>
               <td colspan="5">
                  Aggregate of income of all house property (1+2+3=----) (provide additional papaers if necessary)
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  1
               </td>
               <td colspan="4">(Income from house property 1)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  2
               </td>
               <td colspan="4">(Income from house property 2)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>
                  3
               </td>
               <td colspan="4">(Income from house property 3)</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end 8 -->

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->


      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">
         <colgroup>
            <col style="width: 500px">
            <col style="width: 500px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name :
                     </label>
                     <div class="submitted_data">
                        Mohammad Ali Zinnah Khan
                     </div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Signature & Date
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end  page layout 6 -->

   <!-- start page layout 7 -->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start page header -->
      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Schedule 24C
            </span>
         </h4>
         <div>
            Particulars of income from business or profession <br />
            Annex this schedule to the return of income if you have income business or profession
         </div>
      </div>
      <!-- end page header -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 450px">
            <col style="width: 50px">
            <col style="width: 450px">

         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no">01</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        Assessment Year
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">2</span>
                     </div>
                  </div>
               </td>
               <td class="sl_no">02</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        TIN :
                     </label>
                     <div class="submitted_data">
                        336019506766
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td colspan="3">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Type of main business or profession
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name(s) of the business or profession: (as in trade licence)
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
               <td class="sl_no">05</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Address(es):
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 750px">
            <col style="width: 200px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label" colspan="2">Summary of income</td>
               <td class="label">
                  Amount
               </td>

            </tr>
            <tr>
               <td class="sl_no">06</td>
               <td>
                  Sales/ Turnover/ Receipts
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">07</td>
               <td>
                  Gross Profit
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">08</td>
               <td>
                  General, administrative, selling and other expenses
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">09</td>
               <td>
                  Net Profit(07-08)
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 750px">
            <col style="width: 200px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label" colspan="2">Summary of Balance Sheet</td>
               <td class="label">
                  Amount
               </td>
            </tr>
            <tr>
               <td class="sl_no">10</td>
               <td>
                  Cash in hand & at bank
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">11</td>
               <td>
                  Inventories
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">12</td>
               <td>
                  Fixed assets
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">13</td>
               <td>
                  Other assets
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">14</td>
               <td>
                  Total assets (10+11+12+13)
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">15</td>
               <td>
                  Opening capital
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">16</td>
               <td>
                  Net Profit
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">17</td>
               <td>
                  Withdrawals in the income year
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">18</td>
               <td>
                  Closing capital (15+16-17)
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">19</td>
               <td>
                  Liabilities
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">20</td>
               <td>
                  Total Capital and liabilities (18+19)
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">
         <colgroup>
            <col style="width: 500px">
            <col style="width: 500px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name :
                     </label>
                     <div class="submitted_data">
                        Mohammad Ali Zinnah Khan
                     </div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Signature & Date
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end page layout 7 -->

   <!-- start page layout 8 -->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start page header -->
      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Schedule 24D
            </span>
         </h4>
         <div>
            To be annexd to return by on assessee claiming investment tax credit<br>
            (Attach the proof of claimed investment, contribution, etc)
         </div>
      </div>
      <!-- end page header -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 450px">
            <col style="width: 50px">
            <col style="width: 450px">

         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no">01</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        Assessment Year
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">2</span>
                     </div>
                  </div>
               </td>
               <td class="sl_no">02</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        TIN :
                     </label>
                     <div class="submitted_data">
                        336019506766
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td colspan="3">
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Type of main business or profession
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name(s) of the business or profession: (as in trade licence)
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
               <td class="sl_no">05</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Address(es):
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 700px">
            <col style="width: 200px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label" colspan="3">Particulars of rebatable investment, contribution, etc</td>
               <td class="label">
                  Amount
               </td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td colspan="2">
                  Life insurance premium
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td colspan="2">
                  Contribution to deposit pension scheme (not exceeding allowable limit)
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">05</td>
               <td colspan="2">
                  Investment in approvedsaving Certificate
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">06</td>
               <td colspan="2">
                  Investment in approvedsaving debenture or debenture stock, stock or shares
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">07</td>
               <td colspan="2">
                  Contribution to provident fund to which provident Fund Act, 1925 applies
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">08</td>
               <td colspan="2">
                  Self contribution and employer's contribution to recognized provident fund
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">09</td>
               <td colspan="2">
                  Contribution to Super Annualtion Fund
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">10</td>
               <td colspan="2">
                  Contribution to Benevolent Fund and Group insurance Premium
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">11</td>
               <td colspan="2">
                  Contribution to Zakat Fund
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">12</td>
               <td colspan="2">
                  Others, if any (give details)
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">13</td>
               <td colspan="2">
                  Total allowable investment, contribution, etc.
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no" rowspan="4">14</td>
               <td colspan="2">
                  Eligible amount for rebate (the lesser of 14A. 14B or 14C)
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">
                  14A
               </td>
               <td>
                  Total allowable investment, contributin, etc (as in 13)
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">
                  14B
               </td>
               <td>
                  ....% of the total income [excluding any income for which a tax exemption or a reduced rate is
                  applicable under sub-section (4) of section 44 or any income from any source or sources mentiond in
                  clause (a) of sub-section (2) of section 82c.]
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">
                  14C
               </td>
               <td>
                  1.5 crore
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">15</td>
               <td colspan="2">
                  Amount of tax rebate calculated on eligible amount (Serial14) under section 44(2)(b)
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">
         <colgroup>
            <col style="width: 500px">
            <col style="width: 500px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name :
                     </label>
                     <div class="submitted_data">
                        Mohammad Ali Zinnah Khan
                     </div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Signature & Date
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end page layout 8 -->

   <!-- start page layout 9 -->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start page header -->
      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Statement of assets, libabilities and expenses
            </span>
         </h4>
         <div>
            Under Section 80(1)of the income tax ordinance, 1984 (XXXVI of 1984)
         </div>
      </div>
      <!-- end page header -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:10x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 450px">
            <col style="width: 50px">
            <col style="width: 450px">

         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td colspan="4">
                  <div class="form-card-additional-list list-2">
                     <div>
                        <strong>1. </strong>
                        Mention the amount of assets and liabilities that you have at the
                        last date of the income year. All items shall be at cost value
                        include legal, registration and all other related costs.
                     </div>
                     <div>
                        <strong>2. </strong>
                        If your spouse or minor children and dependensts(s) are not assessee,
                        you have to include their assests and liabilities in your statement.
                     </div>
                     <div>
                        <strong>3. </strong>
                        Schedule 25 is the integral part of this statement if you have business capital or
                        agriculture or non-agricuultural property. provide additional papers if neccessary.
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">01</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Assessment Year
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                     </div>
                  </div>
               </td>
               <td class="sl_no">02</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Statement as on (DD-MM-YYYY)
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                     </div>
                  </div>
               </td>
            </tr>

            <tr>
               <td class="sl_no">03</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name of the Assessee
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
               <td class="sl_no">04</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        TIN :
                     </label>
                     <div class="submitted_data">
                        336019506766
                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:10x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <!-- 6 column -->
         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 240px">
            <col style="width: 240px">
            <col style="width: 240px">
            <col style="width: 180px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label" colspan="5">Particulars</td>
               <td class="label">Amount</td>
            </tr>

            <!-- start sl no 5 -->
            <tr>
               <td rowspan="3" class="sl_no">05</td>
               <td colspan="4" class="label">Business Capital (05A + 05B)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>05A</td>
               <td colspan="3">Business Capital Other than 05B</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>05B</td>
               <td colspan="3">Director's Shareholdings in limited companies (as in schedule 25)</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 5 -->

            <!-- start sl no 6 -->
            <tr>
               <td rowspan="3" class="sl_no">06</td>
               <td colspan="4" class="label">Non-agricultural property</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>06A</td>
               <td colspan="3">Non-agricultural property (as in schedule 25)</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>06B</td>
               <td colspan="3">Advance made for non-agricultural property (as in schedule 25)</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 6 -->

            <!-- start sl no 7 -->
            <tr>
               <td class="sl_no">07</td>
               <td colspan="4">Agricultural property (as in schedule25)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 7 -->

            <!-- start sl no 8 -->
            <tr>
               <td rowspan="6" class="sl_no">08</td>
               <td colspan="4" class="label">Financial assets value (08A+08B+08C+08D+08E)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>08A</td>
               <td colspan="3">Share, debentures etc.</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>08B</td>
               <td colspan="3">Life Insurance Premium</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>08C</td>
               <td colspan="3">Fixed deposit, Term deposits and DPS</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>08D</td>
               <td colspan="3">Loans given to others (Mention name and tin)</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>08E</td>
               <td colspan="3">Other financial assets(give deatils)</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 8 -->

            <!-- start sl no 9 -->
            <tr>
               <td rowspan="4" class="sl_no">09</td>
               <td colspan="4" class="label">Motor Cars (use additional papers if more than two cars)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr class="text-center">
               <td>SL.</td>
               <td>Brand Name</td>
               <td>Engine (CC)</td>
               <td>Registration No.</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="text-right">1</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="text-right">2</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 9 -->

            <!-- start sl no 10 -->
            <tr>
               <td class="sl_no">10</td>
               <td colspan="4">Gold, diamond, gems, and otehr items (mention quantity)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 10 -->

            <!-- start sl no 11 -->
            <tr>
               <td class="sl_no">11</td>
               <td colspan="4">Furniture, equipment and electronics items</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 11 -->

            <!-- start sl no 12 -->
            <tr>
               <td class="sl_no">12</td>
               <td colspan="4">Other assets of significant value (jewellery)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 12 -->

            <!-- start sl no 13 -->
            <tr>
               <td rowspan="5" class="sl_no">13</td>
               <td colspan="4" class="label">Cash and fund outside business (13A+13B+13C+13D)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>13A</td>
               <td colspan="3">Notes and currencies</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>13B</td>
               <td colspan="3">Banks, Cards and electronic cash</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>13C</td>
               <td colspan="3">Provident fund and ohter fund</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>13D</td>
               <td colspan="3">Other deposits, balance and advance(other than 8)</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>

            <!-- end sl no 13 -->

            <!-- start sl no 14 -->
            <tr>
               <td class="sl_no">14</td>
               <td colspan="4">Gross wealth (aggregate of 05 to 13)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 14 -->

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end page layout 9 -->

   <!-- start page layout 10 -->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <!-- 6 column -->
         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 240px">
            <col style="width: 240px">
            <col style="width: 240px">
            <col style="width: 180px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label" colspan="5">Particulars</td>
               <td class="label">Amount</td>
            </tr>

            <!-- start sl no 15 -->
            <tr>
               <td rowspan="4" class="sl_no">15</td>
               <td colspan="4" class="label">Liabilities outside business (15A+15B+15C)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>15A</td>
               <td colspan="3">Borrowdings from banks other finincial instirutions</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>15B</td>
               <td colspan="3">Unsecured loadn (mention name and TIN)</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>15C</td>
               <td colspan="3">Other loans or overdrafts</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 15 -->

            <!-- start sl no 16 -->
            <tr>
               <td class="sl_no">16</td>
               <td colspan="4">Net Wealth (14-15)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 16 -->

            <!-- start sl no 17 -->
            <tr>
               <td class="sl_no">17</td>
               <td colspan="4">
                  Net Wealth at the last date of the pervious income year(previous year pdf adjustment)
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 17 -->

            <!-- start sl no 18 -->
            <tr>
               <td class="sl_no">18</td>
               <td colspan="4">
                  Change in net wealth (16-17)
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 18 -->

            <!-- start sl no 19 -->
            <tr>
               <td rowspan="4" class="sl_no">19</td>
               <td colspan="4" class="label">Other fund outflow during the income year (19A+19B+19C)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>19A</td>
               <td colspan="3">Annual living expenditure and tax payments (as IT-10BB2016)</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>19B</td>
               <td colspan="3">Loss, deductions, expenses, etc. not mentioned in IT-10BB2016</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>19C</td>
               <td colspan="3">Gift, donation and contribution</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 19 -->

            <!-- start sl no 20 -->
            <tr>
               <td class="sl_no">20</td>
               <td colspan="4">
                  Total fund outflow in the income year (18+19)
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 20 -->

            <!-- start sl no 21 -->
            <tr>
               <td rowspan="4" class="sl_no">21</td>
               <td colspan="4" class="label">Sources of fund (21A+21B+21C)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>21A</td>
               <td colspan="3">Income shown in the return</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>21B</td>
               <td colspan="3">Tax exempted income and allowance</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>21C</td>
               <td colspan="3">Other receipts (Gift from father & mother)</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 21 -->

            <!-- start sl no 22 -->
            <tr>
               <td class="sl_no">22</td>
               <td colspan="4">
                  Shortage of fund, if any (20-21)
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 22 -->

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <div class="mb-1"><strong>Verification and signature</strong></div>
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 475px">
            <col style="width: 475px">
         </colgroup>
         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no" rowspan="3">23</td>
               <td colspan="2">
                  <strong>Verification</strong> <br>
                  I solemnly declare that to the best of my
                  knowledge and belief the information given in this retrn and
                  statements and documents annexed or attached here with are
                  correct and complete.
               </td>
            </tr>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name :
                     </label>
                     <div class="submitted_data">Mohammad Ali Zinnah Khan</div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Signature & Date :
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end page layout 10 -->

   <!-- start page layout 11 -->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start page header -->
      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               SCHEDULE 25
            </span>
         </h4>
         <div>
            To be annexed to the statement of Assets, Liabilities and Expenses (IT-10B2016)
         </div>
      </div>
      <!-- end page header -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 450px">
            <col style="width: 50px">
            <col style="width: 450px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no">01</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Assessment Year
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">2</span>
                     </div>
                  </div>
               </td>
               <td class="sl_no">02</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        TIN :
                     </label>
                     <div class="submitted_data">
                        336019506766
                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <!-- 6 column -->
         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 250px">
            <col style="width: 250px">
            <col style="width: 200px">
            <col style="width: 200px">
         </colgroup>

         <!-- start table body -->
         <tbody>

            <!-- start sl no 9 -->
            <tr>
               <td rowspan="6" class="sl_no">03</td>
               <td colspan="3" class="label">Shareholdings in limited companies as director</td>
               <td class="label text-center">
                  No. of shares
               </td>
               <td class="label text-center">
                  Value
               </td>
            </tr>
            <tr>
               <td class="sl_no">01</td>
               <td class="text-right" colspan="2">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">02</td>
               <td class="text-right" colspan="2">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td class="text-right" colspan="2">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td class="text-right" colspan="2">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>

               <td colspan="3">

               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <!-- 6 column -->
         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 250px">
            <col style="width: 250px">
            <col style="width: 200px">
            <col style="width: 200px">
         </colgroup>

         <!-- start table body -->
         <tbody>

            <!-- start sl no 9 -->
            <tr>
               <td rowspan="6" class="sl_no">04</td>
               <td colspan="2" class="label">Non-agricultural property at cost value or any advance made for such property (description, location and size)</td>
               <td class="label text-center">
                  Value at the start of income year
               </td>
               <td class="label text-center">
                  Increased/decreased during the income year
               </td>
               <td class="label text-center">
                  Value at the last date of income year
               </td>
            </tr>
            <tr>
               <td class="sl_no">01</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">02</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>

               <td colspan="2">

               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <!-- 6 column -->
         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 250px">
            <col style="width: 250px">
            <col style="width: 200px">
            <col style="width: 200px">
         </colgroup>

         <!-- start table body -->
         <tbody>

            <!-- start sl no 9 -->
            <tr>
               <td rowspan="6" class="sl_no">05</td>
               <td colspan="2" class="label">Agricultural property at cost value or any advance made for such property (description, location and size)</td>
               <td class="label text-center">
                  Value at the start of income year
               </td>
               <td class="label text-center">
                  Increased/decreased during the income year
               </td>
               <td class="label text-center">
                  Value at the last date of income year
               </td>
            </tr>
            <tr>
               <td class="sl_no">01</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">02</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">03</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td class="sl_no">04</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>

               <td colspan="2">

               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <div class="m-0"><strong>(Provide additional paper if neccessary)</strong></div>
      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 500px">
            <col style="width: 500px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name :
                     </label>
                     <div class="submitted_data">
                        Mohammad Ali Zinnah Khan
                     </div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Signature & Date
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end page layout 11 -->

   <!-- start page layout 12 -->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start page header -->
      <div class="form-header" style="text-align: center;">
         <h4 class="form-title">
            <span style="display: block; margin-bottom: 0; text-transform: uppercase; font-weight: 500;">
               Statement of expenses, relating to lifestyle
            </span>
         </h4>
         <div>
            Under Section 80(2)of the income tax ordinance, 1984 (XXXVI of 1984)
         </div>
      </div>
      <!-- end page header -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 450px">
            <col style="width: 50px">
            <col style="width: 450px">
         </colgroup>

         <!-- start table body -->
         <tbody>

            <tr>
               <td class="sl_no">01</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Assessment Year
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">9</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                     </div>
                  </div>
               </td>
               <td class="sl_no">02</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Statement as on (DD-MM-YYYY)
                     </label>
                     <div class="multiple-input-box-area">
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">1</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">0</span>
                        <span class="box-sm-box submitted_data">2</span>
                        <span class="box-sm-box submitted_data">1</span>
                     </div>
                  </div>
               </td>
            </tr>

            <tr>
               <td class="sl_no">03</td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name of the Assessee
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
               <td class="sl_no">04</td>
               <td>
                  <div class="form-group">
                     <label class="label d-block">
                        TIN :
                     </label>
                     <div class="submitted_data">
                        336019506766
                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <!-- 6 column -->
         <colgroup>
            <col style="width: 50px">
            <col style="width: 50px">
            <col style="width: 240px">
            <col style="width: 240px">
            <col style="width: 210px">
            <col style="width: 210px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label" colspan="4">Particulars</td>
               <td class="label">Amount</td>
               <td class="label">Comment</td>
            </tr>

            <!-- start sl no 5 -->
            <tr>
               <td class="sl_no">05</td>
               <td colspan="3">Business Capital (05A + 05B)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td>
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 5 -->

            <!-- start sl no 6 -->
            <tr>
               <td class="sl_no">06</td>
               <td colspan="3">Housing expense</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td>
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 6 -->

            <!-- start sl no 7 -->
            <tr>
               <td rowspan="3" class="sl_no">07</td>
               <td colspan="3" class="label">Auto and transportation expense (07A+07B)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>07A</td>
               <td colspan="2">Driver's salary, fuel and maintenance</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>07B</td>
               <td colspan="2">Other transportation</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 7 -->

            <!-- start sl no 7 -->
            <tr>
               <td rowspan="5" class="sl_no">08</td>
               <td colspan="3" class="label">Household and utility expenses (08A+08B+08C+08D)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>08A</td>
               <td colspan="2">Electicity</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>08B</td>
               <td colspan="2">Gas, water, sewer and garbage</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>08C</td>
               <td colspan="2">Phone, internet, TV channels subscription</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>08D</td>
               <td colspan="2">Home-support staff and other expenses</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 8 -->

            <!-- start sl no 9 -->
            <tr>
               <td class="sl_no">09</td>
               <td colspan="3">children's & wife education expenses</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td>
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 9 -->

            <!-- start sl no 10 -->
            <tr>
               <td rowspan="5" class="sl_no">10</td>
               <td colspan="3" class="label">Special expenses (10A+10B+10C+10D)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>10A</td>
               <td colspan="2">Festival, party, events and gift</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>10B</td>
               <td colspan="2">Domestic and overseas tour, holiday, etc</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>10C</td>
               <td colspan="2">Donation, philantropy etc</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>10D</td>
               <td colspan="2">Other special expenses</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 10 -->

            <!-- start sl no 11 -->
            <tr>
               <td class="sl_no">11</td>
               <td colspan="3">Any other expenses</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td>
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 11 -->

            <!-- start sl no 12 -->
            <tr>
               <td class="sl_no">12</td>
               <td colspan="3">Total expense relating to lifestyle (05+06+07+08+09+10+11)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td>
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 12 -->

            <!-- start sl no 13 -->
            <tr>
               <td rowspan="3" class="sl_no">13</td>
               <td colspan="3" class="label">Payment of tax, charges, etc .(13A+13B)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <tr>
               <td>13A</td>
               <td colspan="2">Payment of tax at source</td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
               <td>
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>13B</td>
               <td colspan="2">Payment of tax, surcharge or other amount</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <!-- end sl no 13 -->

            <!-- start sl no 14 -->
            <tr>
               <td class="sl_no">14</td>
               <td colspan="3">Total amount of expense and tax (12+13)</td>
               <td class="text-right">
                  <div class="submitted_data"></div>
               </td>
               <td>
                  <div class="submitted_data"></div>
               </td>
            </tr>
            <!-- end sl no 14 -->

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <div class="mb-1"><strong>Verification and signature</strong></div>
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 50px">
            <col style="width: 475px">
            <col style="width: 475px">
         </colgroup>
         <!-- start table body -->
         <tbody>
            <tr>
               <td class="sl_no" rowspan="3">15</td>
               <td colspan="2">
                  <strong>Verification</strong> <br>
                  I solemnly declare that to the best of my
                  knowledge and belief the information given in this retrn and
                  statements and documents annexed or attached here with are
                  correct and complete.
               </td>
            </tr>
            <tr>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Name :
                     </label>
                     <div class="submitted_data">Mohammad Ali Zinnah Khan</div>
                  </div>
               </td>
               <td>
                  <div class="form-group">
                     <label class="label mb-2 d-block">
                        Signature & Date :
                     </label>
                     <div class="submitted_data">

                     </div>
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end page layout 12 -->

   <!-- start page layout 13 -->
   <div class="print-page-layout" style="margin: 0 auto; max-width: 1000px; height:1200px ">

      <!-- start page header -->
      <div class="mb-4 text-center">
         <div class="mb-0"><strong>Mohammad Ali Zinnah Khan</strong></div>
         <div class="mb-0"><strong>336019506766</strong></div>
         <div class="mb-0"><strong>Assesment Year : 2019-2020</strong></div>
         <div class="mb-0"><strong>Computation Sheet</strong></div>
      </div>
      <!-- end page header -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 250px">
            <col style="width: 250px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr>
               <td class="label">Total Taxable income</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->

      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 400px">
            <col style="width: 240px">
            <col style="width: 120px">
            <col style="width: 240px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label"></td>
               <td class="label">Income on</td>
               <td class="label">Rate</td>
               <td class="label">Tax</td>
            </tr>
            <tr>
               <td class="label">On First</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="label">Next 4,00,000</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="label"> Next 5,00,000</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="label">Next 6,00,000</td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td class="label"></td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="label"></td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="label" colspan="3">Total Gross tax before investment Tax rebate</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="label" colspan="3">Less: Investment Tax Rebate</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="label" colspan="3">Tax Liabilities after Tax Rebate</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="label" colspan="3">Less: Tax Deduction ata source on salary</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="label" colspan="3">Less: Tax paid through challan</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
            <tr>
               <td class="label" colspan="3">Less: Tax Liabilities tax u/s-74</td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>
         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

      <!-- start table -->
      <table class="table table-bordered" style=" table-layout: fixed; width: 1000px; margin:20x 0; border: none;  border-collapse: collapse;" cellspacing="0" cellpadding="0">

         <colgroup>
            <col style="width: 280px">
            <col style="width: 200px">
            <col style="width: 200px">
            <col style="width: 120px">
            <col style="width: 200px">
         </colgroup>

         <!-- start table body -->
         <tbody>
            <tr class="text-center">
               <td class="label" colspan="5">
                  Investment tax rebate calculation
               </td>
            </tr>
            <tr class="text-center">
               <td class="label">Particulars</td>
               <td class="label">Amount</td>
               <td class="label">Amount</td>
               <td class="label">Rate</td>
               <td class="label">Rebate Amount</td>
            </tr>
            <tr>
               <td>
                  Actual Investment
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  25% of Total income
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  TK. 1,50,00,000/-
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
            </tr>
            <tr>
               <td>

               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">

                  </div>
               </td>
               <td class="text-right">
                  <div class="submitted_data">
                     -
                  </div>
               </td>
            </tr>

         </tbody>
         <!-- end table body -->
      </table>
      <!-- end table -->

   </div>
   <!-- end page layout 13 -->

</body>

</html>